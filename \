//#include <vulkan/vulkan.h>

#define P_CHECK(p) (assert(p != NULL))

#include "base/vulkan_header.h"  // custom vulkan header 

#include <fstream>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "base/base_vrulk.h"
#include "base/queue_family.h"
/* Extenstions */
#include "base/ext/ext.h"
#include "base/ext/vulk_surface.h"
#include "base/ext/vulk_swapchain.h"

/* Engine */
#include "base/vrulk_engine.h"
#include "base/vertex.h"

#define DEBUG 1
#if DEBUG
    const bool enableValidationLayers = true;
#else
    const bool enableValidationLayers = false
#endif

FILE* fDebug;
bool framebuffer_resized = false;

const int MAX_FRAMES_IN_FLIGHT = 2;

// Recreates the engine and stores the new engine in 'engine'
//void recreate_engine(VkDevice* device, VrulkEngine** engine, VrulkEngineTools* tools);

const std::vector<Vertex> vertices = {
    {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
    {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
    {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
    {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}}
};

const std::vector<uint16_t> indices = {
    0, 1, 2, 2, 3, 0
};

static std::vector<char> readFile(const char *file_path) {
    std::ifstream file(file_path, std::ios::ate | std::ios::binary);

    if (!file.is_open() ) {
        throw std::runtime_error("failed to open file!");
    }
    size_t fileSize = (size_t) file.tellg();
    std::vector<char> buffer(fileSize);

    file.seekg(0);
    file.read(buffer.data(), fileSize);

    file.close();

    return buffer;

}

void drawFrame(VkPhysicalDevice physicalDevice, VkDevice* device, 
               VkQueue* graphicsQueue, VkQueue* presentQueue, bool *framebufferResized,
               VrulkSwapchain** swapchain,
               VrulkPipeline** renderer,
               VrulkCommands** commands,
               VrulkEngineTools* tools,
               VkBuffer* vertex_buffer,
               std::vector<VkSemaphore> imageSemaphore, std::vector<VkSemaphore> renderSemaphore,
               std::vector<VkFence> flightFences, std::vector<VkFence> imagesInFlight,
               size_t *currentFrame);

void print_error(const char *msg) {
    printf("ERROR: %s\n", msg);
}

void debug_cleanup() {
    #if DEBUG
        fclose(fDebug);
    #endif
}

void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
    PFN_vkDestroyDebugUtilsMessengerEXT func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr) {
        func(instance, debugMessenger, pAllocator);
    }
}

void checkDeviceGroupProperties(VkInstance instance) {
    // query physical device group
    // ---------------------------
    uint32_t deviceGroupCount = 0;
    VkPhysicalDeviceGroupProperties *groupProperties;

    vkEnumeratePhysicalDeviceGroups(instance, &deviceGroupCount, nullptr);
    assert(deviceGroupCount);
    groupProperties = (VkPhysicalDeviceGroupProperties *) malloc(sizeof(VkPhysicalDeviceGroupProperties) * deviceGroupCount);

    P_CHECK(groupProperties);
    if (vkEnumeratePhysicalDeviceGroups(instance, &deviceGroupCount, groupProperties) != VK_SUCCESS) {
#if DEBUG
            fputs("ERROR: Unable to get physical device groups\n", fDebug);
#endif
        exit(1);
    }

#if DEBUG
    for(int i = 0; i < deviceGroupCount; i++) {
        fprintf(fDebug,"Devices in group: %d\n", groupProperties[i].physicalDeviceCount);
    }
#endif
    free(groupProperties);

}


VkQueue getQueue(VkDevice device, struct QueueFamily* queueFamily) {
    VkQueue queue;
    vkGetDeviceQueue(device, queueFamily->familyIndex, 0, &queue);

    return queue;
}

static void framebuffer_resize_callback(GLFWwindow* window, int width, int height) {
    printf("FrameBUFFER Resized\n");
    framebuffer_resized = true;
}

int main()
{
    const int WIDTH = 800;
    const int HEIGHT = 600;

    glfwInit();
    // GLFW window
    // -----------
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    //glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    GLFWwindow* window = NULL;
    window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", nullptr, nullptr);
    if (window == NULL) {
        printf("Error: Can't create window\n");
        exit(1);
    }
    glfwSetFramebufferSizeCallback(window, framebuffer_resize_callback);


#if DEBUG
    fDebug = fopen("dlog.log", "w");
#endif

    
#if DEBUG
    uint32_t api_version = 0;
    if (vkEnumerateInstanceVersion(&api_version) == VK_SUCCESS) {
        fprintf(fDebug,"Vulkan version: %d.%d.%d\n", VK_VERSION_MAJOR(api_version), VK_VERSION_MINOR(api_version), VK_VERSION_PATCH(api_version));
    }
#endif

    std::vector<const char *> use_extensions = getInstanceExtensions(&fDebug);

    VkInstance instance = createInstance("My Vulkan Renderer", 1, 
                                         "Vrulk", 1, 
                                         use_extensions,
                                         true, &fDebug);
    if (instance == VK_NULL_HANDLE) {
        fputs("Unable to create Instance\n", fDebug);
    } else {
        fputs("Instance created\n", fDebug);
    }


    VkDebugUtilsMessengerEXT debugMessenger = setupDebugMessenger(instance, &fDebug);
    if (debugMessenger == VK_NULL_HANDLE) {
        fputs("Unable to create dMES\n", fDebug);
    }
    
    VkPhysicalDevice physicalDevice = createPhysicalDevice(instance, &fDebug);


#if DEBUG
    if (physicalDevice == VK_NULL_HANDLE) {
        fputs("No device found\n", fDebug);
    } else {
        fputs("Physical Device found\n", fDebug);
    }
#endif

    
    // Window Surface creation
   // -----------------------
   VkSurfaceKHR surface = VK_NULL_HANDLE;
   if (glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS) {
       printf("Unable to create window surface\n");
       exit(1);
   } 

   struct QueueFamily presentFamily = surfaceSupport(physicalDevice, surface);
   if (presentFamily.filled == false) {
        fprintf(fDebug, "No present queue supports surface\n");
        exit(1);
   }

   //*** Extensions to use ***//
   std::vector<struct QueueFamily> queue_extensions;
   std::vector<const char *> device_extensions;

   //* Fill queues *//
    queue_extensions.push_back(presentFamily); 

    //* Fill device extensions *//
    device_extensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

   
   VkDevice device = createLogicalDevice(physicalDevice, queue_extensions, device_extensions, (&stdout)); 

   if (device == VK_NULL_HANDLE) {
       printf("unable to create logical device\n");
       exit(1);
   } else {
       printf("logical device created\n");
   }
    
   
   // queue handles
   // -------------
   struct QueueFamily graphicFamily = findQueue(physicalDevice, VK_QUEUE_GRAPHICS_BIT);    

   VkQueue graphicsQueue = getQueue(device, &graphicFamily);
   if (graphicsQueue == VK_NULL_HANDLE) {
       print_error("Could not create Graphics QUeue");
       exit(1);
   }
   VkQueue presentQueue = getQueue(device, &presentFamily);

   
   
   // Swapchain Creation
   // ------------------

   // queues that will interact with swapchain
    std::set<uint32_t> queue_set;
    queue_set.insert(graphicFamily.familyIndex);
    queue_set.insert(presentFamily.familyIndex);

    // shaders stage
    
    std::vector<char> vertexShaderCode   =readFile("shaders/vert.spv");
    std::vector<char> fragmentShaderCode = readFile("shaders/frag.spv");

    VkShaderModule vertexModule   = createShaderModule(device, vertexShaderCode);
    VkShaderModule fragmentModule = createShaderModule(device, fragmentShaderCode);

    if (vertexModule == VK_NULL_HANDLE || fragmentModule == VK_NULL_HANDLE) {
        printf("unable to create shader module\n");
        exit(1);
    }

    

    VkPipelineShaderStageCreateInfo vertexShaderStage = createShaderStage(device, vertexModule, "main", VK_SHADER_STAGE_VERTEX_BIT);
    VkPipelineShaderStageCreateInfo fragmentShaderStage = createShaderStage(device, fragmentModule, "main", VK_SHADER_STAGE_FRAGMENT_BIT);

    std::vector<VkPipelineShaderStageCreateInfo> shader_stages = {vertexShaderStage, fragmentShaderStage};

    VrulkEngineTools* engine_tools = create_vrulk_tools(queue_set, shader_stages, surface, 2, graphicFamily.familyIndex, &window, vertices);
    if (engine_tools == nullptr) {
        print_error("Unable to create engine tools");
    } else {
        printf("Valid engine tools\n");
    }

    VrulkSwapchain* swapchain = create_vrulk_swapchain(physicalDevice, device, queue_set, surface, &window);
    VrulkPipeline* renderer = create_vrulk_pipeline(device, swapchain, engine_tools->shader_stages.data(), engine_tools->shader_amount, engine_tools->queue_index);
    VrulkCommands* commands = create_vrulk_commands(device, engine_tools->queue_index, renderer->framebuffers.size());

    VkDeviceSize vertices_size = sizeof(vertices[0]) * vertices.size();
    VkBuffer staging_buffer = createBuffer(device, vertices_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
    VkDeviceMemory* staging_buffer_memory = allocateBufferMemory(physicalDevice, device, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &staging_buffer);
    if (staging_buffer_memory == VK_NULL_HANDLE) {
        exit(1);
    }
    bindBuffer(device, vertices, &staging_buffer, staging_buffer_memory);
    mapData(device, staging_buffer_memory, (uint32_t) vertices_size, (void *)vertices.data());

    VkBuffer vertexBuffer = createVertexBuffer(device, vertices.size(), VK_BUFFER_USAGE_TRANSFER_DST_BIT);
    VkDeviceMemory* vertex_buffer_memory = allocateBufferMemory(physicalDevice, device, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &vertexBuffer);
    bindBuffer(device, vertices, &vertexBuffer, vertex_buffer_memory);


    copyBuffer(&device, *commands->graphic_pool, &graphicsQueue, &staging_buffer, &vertexBuffer, sizeof(vertices[0]) * vertices.size());


    vkDestroyBuffer(device, staging_buffer, nullptr);
    vkFreeMemory(device, *staging_buffer_memory, nullptr);
    free(staging_buffer_memory);


    VkResult status = cmd_Render(commands->command_buffer, *renderer->render_pass, renderer->framebuffers, swapchain->extent, *renderer->graphics_pipeline, vertices, &vertexBuffer);

    if (status != VK_SUCCESS) {
        exit(1);
    }

    
    VkSemaphoreCreateInfo semaphoreInfo;
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphoreInfo.pNext = VK_NULL_HANDLE;
    semaphoreInfo.flags = 0;

    VkFenceCreateInfo fenceInfo;
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.pNext = VK_NULL_HANDLE;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT; // initialize to signaled state. 


    std::vector<VkSemaphore> imageAvailableSemaphore;
    std::vector<VkSemaphore> renderFinishedSemaphore;
    std::vector<VkFence> inFlightFences;
    std::vector<VkFence> imagesInFlight;

    imageAvailableSemaphore.resize(MAX_FRAMES_IN_FLIGHT);
    renderFinishedSemaphore.resize(MAX_FRAMES_IN_FLIGHT);

    inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
    imagesInFlight.resize(swapchain->images.size(), VK_NULL_HANDLE);

    for(int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphore[i]) != VK_SUCCESS ||
            vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphore[i]) != VK_SUCCESS ||
            vkCreateFence(device, &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS) {
            print_error("Could not create semaphores");
        }
    }
    
        
    
    size_t current_frame = 0;
    // main loop
    // ---------
    while(!glfwWindowShouldClose(window)) {
        glfwPollEvents();
        drawFrame(physicalDevice, 
                  &device, 
                  &graphicsQueue, 
                  &presentQueue, 
                  &framebuffer_resized, 
                  &swapchain,
                  &renderer,
                  &commands,
                  engine_tools, 
                  &vertexBuffer,
                  imageAvailableSemaphore, renderFinishedSemaphore, 
                  inFlightFences, imagesInFlight, &current_frame);
    }
    

    vkDeviceWaitIdle(device);
    




    // cleanup
    //

    
    for(int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(device, renderFinishedSemaphore[i], nullptr);
        vkDestroySemaphore(device, imageAvailableSemaphore[i], nullptr);
        vkDestroyFence(device, inFlightFences[i], nullptr);
    }
    

    destroy_vrulk_commands(device, commands);
    destroy_vrulk_pipeline(device, renderer);
    destroy_swapchain(device, swapchain);

    vkDestroyBuffer(device, vertexBuffer, nullptr);
    vkFreeMemory(device, *vertex_buffer_memory, nullptr);
    free(vertex_buffer_memory);

    

    
    vkDestroyShaderModule(device, fragmentModule, nullptr);
    vkDestroyShaderModule(device, vertexModule, nullptr);
    
    

    if (enableValidationLayers) {
        DestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr); 
    }
    // Queue's get destroyed when the logical device gets destroyed
    vkDestroyDevice(device, nullptr);
    

    vkDestroySurfaceKHR(instance, surface, nullptr);
    vkDestroyInstance(instance, nullptr);
    delete(engine_tools);
    

    /*
    glfwDestroyWindow(window);
    glfwTerminate();
    */

#if DEBUG
    fclose(fDebug);
#endif
}

void recreate_engine(VkPhysicalDevice physicalDevice, 
                     VkDevice* device, 
                     VrulkSwapchain** swapchain,
                     VrulkPipeline** renderer,
                     VrulkCommands** commands,
                     VrulkEngineTools* tools,
                     VkBuffer* vertex_buffer) {
    int width = 0, height = 0;
    glfwGetFramebufferSize(*tools->window, &width, &height);
    while (width == 0 || height == 0) {
        glfwGetFramebufferSize(*tools->window, &width, &height);
        glfwWaitEvents();
    }

    vkDeviceWaitIdle(*device);

    destroy_vrulk_commands(*device, *commands);
    destroy_vrulk_pipeline(*device, *renderer);
    destroy_swapchain(*device, *swapchain);

    *swapchain = create_vrulk_swapchain(physicalDevice, *device, tools->queue_set, tools->surface, tools->window);
    *renderer = create_vrulk_pipeline(*device, *swapchain, tools->shader_stages.data(), tools->shader_amount, tools->queue_index);
    *commands = create_vrulk_commands(*device, tools->queue_index, (*renderer)->framebuffers.size());


    cmd_Render((*commands)->command_buffer, 
               *(*renderer)->render_pass, 
               (*renderer)->framebuffers, 
               (*swapchain)->extent, 
               *(*renderer)->graphics_pipeline,
               tools->vertices,
               vertex_buffer);

}

void drawFrame(VkPhysicalDevice physicalDevice, VkDevice* device, 
               VkQueue* graphicsQueue, VkQueue* presentQueue, 
               bool *framebufferResized, 
               VrulkSwapchain** swapchain,
               VrulkPipeline** renderer,
               VrulkCommands** commands,
               VrulkEngineTools* tools,
               VkBuffer* vertex_buffer,
               std::vector<VkSemaphore> imageSemaphore, std::vector<VkSemaphore> renderSemaphore,
               std::vector<VkFence> flightFences, std::vector<VkFence> imagesInFlight,
               size_t *currentFrame) 
{
    // wait for current frame to be available

    vkWaitForFences(*device, 1, &flightFences[*currentFrame], VK_TRUE, UINT64_MAX);


    // acquire image from swap chain
    uint32_t imageIndex;
    VkResult result =  vkAcquireNextImageKHR(*device, *(*swapchain)->swapchain, UINT32_MAX, imageSemaphore[*currentFrame], nullptr, &imageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        printf("RECREATING\n");
        recreate_engine(physicalDevice, device, swapchain, renderer, commands, tools, vertex_buffer);
        return;
    } else if (result !=VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        exit(1);
    }

    // Check if a previous frame is using this image (i.e. there is its fence to wait on
    if (imagesInFlight[imageIndex] != VK_NULL_HANDLE) {
        vkWaitForFences(*device, 1, &imagesInFlight[imageIndex], VK_TRUE, UINT64_MAX);
    }

    // Mark the image as now being in use by this frame
    imagesInFlight[imageIndex] = flightFences[*currentFrame];

    VkSubmitInfo submitInfo;
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.pNext = VK_NULL_HANDLE;

    VkSemaphore waitSemaphores[] = {imageSemaphore[*currentFrame]};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;
    // specify command buffers to submit for execution
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &(*commands)->command_buffer[imageIndex];

    // signal semaphores once command buffer has finished executing
    VkSemaphore signalSemaphores[] = {renderSemaphore[*currentFrame]};
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    vkResetFences(*device, 1, &flightFences[*currentFrame]);

    if (vkQueueSubmit(*graphicsQueue, 1, &submitInfo, flightFences[*currentFrame]) != VK_SUCCESS) {
        print_error("Unable to submit queue");
        exit(1);
    }

    VkPresentInfoKHR presentInfo;
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.pNext = VK_NULL_HANDLE;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;
    VkSwapchainKHR swapChains[] = {*(*swapchain)->swapchain};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = nullptr;

    result = vkQueuePresentKHR(*presentQueue, &presentInfo);
    
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || *framebufferResized) {
        *framebufferResized = false;
        printf("Recreating!!!\n");
        recreate_engine(physicalDevice, device, swapchain, renderer, commands, tools, vertex_buffer);
    } else if (result != VK_SUCCESS) {
        exit(1);
    }

    vkQueueWaitIdle(*presentQueue);

    *currentFrame = (*currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}



