LDFLAGS = -lglfw -lvulkan -ldl -lpthread -lX11 -lXxf86vm -lXrandr -lXi
STB_INCLUDE_PATH = -I/home/jonasm/Documents/Programming_World/Libraries/
TINYOBJ_INCLUDE_PATH = /home/jonasm/Documents/Programming_World/Libraries/tinyobjloader/
STB_IMAGE = /home/jonasm/Documents/Programming_World/Libraries/
VECTOR_FILE = ~/Documents/Programming_World/Libraries/vector



CFLAGS = -std=c++17 -Wall -O3 -I$(TINYOBJ_INCLUDE_PATH)

multi:
	$(MAKE) -j4 Triangle

Triangle: src/main.cpp
	g++ $(CFLAGS) -o vulkan src/main.cpp $(STB_INCLUDE_PATH) -I$(STB_IMAGE) -I$(TINYOBJ_INCLUDE_PATH) $(LDFLAGS) $(wildcard src/base/*.cpp) $(wildcard src/base/ext/*.cpp)

.PHONY: test clean

LogicalDevice: src/examples/logical_device.cpp
	g++ $(CFLAGS) -o logical_device src/examples/logical_device.cpp $(STB_INCLUDE_PATH)/stb_image.h $(LDFLAGS) $(wildcard src/base/*.cpp) 


test: Triangle
	./vulkan

clean:
	rm -f vulkan
