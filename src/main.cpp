//#include <vulkan/vulkan.h>

#define P_CHECK(p) (assert(p != NULL))

#include "base/vulkan_header.h"  // custom vulkan header 
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


#include <chrono>

#include <fstream>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "base/base_vrulk.h"
#include "base/queue_family.h"
/* Extenstions */
#include "base/ext/ext.h"
#include "base/ext/vulk_surface.h"
#include "base/ext/vulk_swapchain.h"

/* Engine */
#include "base/vrulk_engine.h"
#include "base/vrulk_models.h"
#include "base/vertex.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>


#define DEBUG 1
#if DEBUG
    const bool enableValidationLayers = true;
#else
    const bool enableValidationLayers = false
#endif

FILE* fDebug;
bool framebuffer_resized = false;

const int MAX_FRAMES_IN_FLIGHT = 2;

// Recreates the engine and stores the new engine in 'engine'
//void recreate_engine(VkDevice* device, VrulkEngine** engine, VrulkEngineTools* tools);
/*
const std::vector<Vertex> vertices = {
    {{0.0f, -0.5f}, {1.0f, 0.0f, 0.0f}},
    {{0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}},
    {{-0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}}
};
*/

struct UniformBufferObject {
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
};

const std::vector<Vertex> vertices = {
    {{-0.5f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
    {{0.5f, -0.5f, 0.0f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
    {{0.5f, 0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
    {{-0.5f, 0.5f, 0.0f}, {1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},

    {{-0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
    {{0.5f, -0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
    {{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
    {{-0.5f, 0.5f, -0.5f}, {1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}}
};

const std::vector<uint16_t> indices = {
    0, 1, 2, 2, 3, 0,
    4, 5, 6, 6, 7, 4
};

struct texture_data {
    int width;
    int height;
    int channels;
};

void load_texture(const char* path, unsigned char** out_data, VkDeviceSize* out_size, struct texture_data* t_data) {
    int width, height, channels;
    *out_data = stbi_load(path, &width, &height, &channels, STBI_rgb_alpha);

    *out_size = width * height * 4;

    // prepare texture structure
    t_data->width = width;
    t_data->height = height;
    t_data->channels = channels;

    if (!(*out_data)) {
        out_data = nullptr;
    }
    
}

static std::vector<char> readFile(const char *file_path) {
    std::ifstream file(file_path, std::ios::ate | std::ios::binary);

    if (!file.is_open() ) {
        throw std::runtime_error("failed to open file!");
    }
    size_t fileSize = (size_t) file.tellg();
    std::vector<char> buffer(fileSize);

    file.seekg(0);
    file.read(buffer.data(), fileSize);

    file.close();

    return buffer;

}

void drawFrame(VkPhysicalDevice physicalDevice, VkDevice* device, 
               VkQueue* graphicsQueue, VkQueue* presentQueue, bool *framebufferResized,
               VrulkSwapchain** swapchain,
               VrulkPipeline** renderer,
               VrulkCommands** commands,
               VrulkBuffer** uniforms,
               VrulkEngineTools* tools,
               VrulkMesh* model,
               VrulkBuffer* vertex_buffer,
               VrulkBuffer* index_buffer,
               VrulkTexture* texture,
               VkDescriptorSetLayout* descriptor_set_layout,
               std::vector<VkSemaphore> imageSemaphore, std::vector<VkSemaphore> renderSemaphore,
               std::vector<VkFence> flightFences, std::vector<VkFence> imagesInFlight,
               size_t *currentFrame);

void print_error(const char *msg) {
    printf("ERROR: %s\n", msg);
}

void debug_cleanup() {
    #if DEBUG
        fclose(fDebug);
    #endif
}

void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
    PFN_vkDestroyDebugUtilsMessengerEXT func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr) {
        func(instance, debugMessenger, pAllocator);
    }
}

void checkDeviceGroupProperties(VkInstance instance) {
    // query physical device group
    // ---------------------------
    uint32_t deviceGroupCount = 0;
    VkPhysicalDeviceGroupProperties *groupProperties;

    vkEnumeratePhysicalDeviceGroups(instance, &deviceGroupCount, nullptr);
    assert(deviceGroupCount);
    groupProperties = (VkPhysicalDeviceGroupProperties *) malloc(sizeof(VkPhysicalDeviceGroupProperties) * deviceGroupCount);

    P_CHECK(groupProperties);
    if (vkEnumeratePhysicalDeviceGroups(instance, &deviceGroupCount, groupProperties) != VK_SUCCESS) {
#if DEBUG
            fputs("ERROR: Unable to get physical device groups\n", fDebug);
#endif
        exit(1);
    }

#if DEBUG
    for(uint32_t i = 0; i < deviceGroupCount; i++) {
        fprintf(fDebug,"Devices in group: %d\n", groupProperties[i].physicalDeviceCount);
    }
#endif
    free(groupProperties);

}


VkQueue getQueue(VkDevice device, struct QueueFamily* queueFamily) {
    VkQueue queue;
    vkGetDeviceQueue(device, queueFamily->familyIndex, 0, &queue);

    return queue;
}

static void framebuffer_resize_callback(GLFWwindow* window, int width, int height) {
    printf("FrameBUFFER Resized\n");
    framebuffer_resized = true;
}

void updateUniformBuffer(uint32_t currentImage);

int main()
{
    const int WIDTH = 800;
    const int HEIGHT = 600;

    glfwInit();
    // GLFW window
    // -----------
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    //glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    GLFWwindow* window = NULL;
    window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", nullptr, nullptr);
    if (window == NULL) {
        printf("Error: Can't create window\n");
        exit(1);
    }
    glfwSetFramebufferSizeCallback(window, framebuffer_resize_callback);


#if DEBUG
    fDebug = fopen("dlog.log", "w");
#endif

    
#if DEBUG
    uint32_t api_version = 0;
    if (vkEnumerateInstanceVersion(&api_version) == VK_SUCCESS) {
        fprintf(fDebug,"Vulkan version: %d.%d.%d\n", VK_VERSION_MAJOR(api_version), VK_VERSION_MINOR(api_version), VK_VERSION_PATCH(api_version));
    }
#endif

    std::vector<const char *> use_extensions = getInstanceExtensions(&fDebug);

    VkInstance instance = createInstance("My Vulkan Renderer", 1, 
                                         "Vrulk", 1, 
                                         use_extensions,
                                         true, &fDebug);
    if (instance == VK_NULL_HANDLE) {
        fputs("Unable to create Instance\n", fDebug);
    } else {
        fputs("Instance created\n", fDebug);
    }


    VkDebugUtilsMessengerEXT debugMessenger = setupDebugMessenger(instance, &fDebug);
    if (debugMessenger == VK_NULL_HANDLE) {
        fputs("Unable to create dMES\n", fDebug);
    }
    
    VkPhysicalDevice physicalDevice = createPhysicalDevice(instance, &fDebug);


#if DEBUG
    if (physicalDevice == VK_NULL_HANDLE) {
        fputs("No device found\n", fDebug);
    } else {
        fputs("Physical Device found\n", fDebug);
    }
#endif

    
    // Window Surface creation
   // -----------------------
   VkSurfaceKHR surface = VK_NULL_HANDLE;
   if (glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS) {
       printf("Unable to create window surface\n");
       exit(1);
   } 

   struct QueueFamily presentFamily = surfaceSupport(physicalDevice, surface);
   if (presentFamily.filled == false) {
        fprintf(fDebug, "No present queue supports surface\n");
        exit(1);
   }

   //*** Extensions to use ***//
   std::vector<struct QueueFamily> queue_extensions;
   std::vector<const char *> device_extensions;

   //* Fill queues *//
    queue_extensions.push_back(presentFamily); 

    //* Fill device extensions *//
    device_extensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

   
   VkDevice device = createLogicalDevice(physicalDevice, queue_extensions, device_extensions, (&stdout)); 

   if (device == VK_NULL_HANDLE) {
       printf("unable to create logical device\n");
       exit(1);
   } else {
       printf("logical device created\n");
   }
    
   
   // queue handles
   // -------------
   struct QueueFamily graphicFamily = findQueue(physicalDevice, VK_QUEUE_GRAPHICS_BIT);    

   VkQueue graphicsQueue = getQueue(device, &graphicFamily);
   if (graphicsQueue == VK_NULL_HANDLE) {
       print_error("Could not create Graphics QUeue");
       exit(1);
   }
   VkQueue presentQueue = getQueue(device, &presentFamily);

   
   
   // Swapchain Creation
   // ------------------

   // queues that will interact with swapchain
    std::set<uint32_t> queue_set;
    queue_set.insert(graphicFamily.familyIndex);
    queue_set.insert(presentFamily.familyIndex);

    // shaders stage
    
    std::vector<char> vertexShaderCode   =readFile("shaders/vert.spv");
    std::vector<char> fragmentShaderCode = readFile("shaders/frag.spv");

    VkShaderModule vertexModule   = createShaderModule(device, vertexShaderCode);
    VkShaderModule fragmentModule = createShaderModule(device, fragmentShaderCode);

    if (vertexModule == VK_NULL_HANDLE || fragmentModule == VK_NULL_HANDLE) {
        printf("unable to create shader module\n");
        exit(1);
    }

    
    VkPipelineShaderStageCreateInfo vertexShaderStage = createShaderStage(device, vertexModule, "main", VK_SHADER_STAGE_VERTEX_BIT);
    VkPipelineShaderStageCreateInfo fragmentShaderStage = createShaderStage(device, fragmentModule, "main", VK_SHADER_STAGE_FRAGMENT_BIT);

    std::vector<VkPipelineShaderStageCreateInfo> shader_stages = {vertexShaderStage, fragmentShaderStage};

    VrulkEngineTools* engine_tools = create_vrulk_tools(queue_set, shader_stages, surface, 2, graphicFamily.familyIndex, &window, vertices, indices);
    if (engine_tools == nullptr) {
        print_error("Unable to create engine tools");
    } else {
        printf("Valid engine tools\n");
    }

    VrulkSwapchain *swapchain = new VrulkSwapchain(physicalDevice, device, queue_set, surface, &window);
    // Create a descriptor set layout
    std::vector<VkDescriptorSetLayoutBinding> bindings;
    bindings.resize(2);
    bindings[0] = create_descriptor_set_layout_binding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT);
    bindings[1] = create_descriptor_set_layout_binding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, VK_SHADER_STAGE_FRAGMENT_BIT);
    /* Passess */
    VkDescriptorSetLayout descriptorSetLayout = create_descriptor_set_layout(device, bindings);

    VrulkPipeline *renderer = new VrulkPipeline(physicalDevice, device,
                           *swapchain,
                           &descriptorSetLayout,
                           engine_tools->shader_stages.data(),
                           engine_tools->shader_amount,
                           engine_tools->queue_index,
                           getMaxUseableCount(physicalDevice));
    VrulkCommands *commands = new VrulkCommands(device, engine_tools->queue_index, renderer->frame_buffers.size());

    VkBuffer index_buffer;
    VkDeviceMemory* index_buffer_memory = (VkDeviceMemory *) malloc(sizeof(VkDeviceMemory));
    VkDeviceSize index_size = sizeof(indices[0]) * indices.size();

    create_mapped_buffer(physicalDevice, device, 
                         commands->pool, &graphicsQueue, 
                         index_size, 
                         (VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT), 
                         &index_buffer, 
                         index_buffer_memory, (void *)indices.data());

    // Uniform buffers
    // ---------------

    VkDeviceSize bufferSize = sizeof(UniformBufferObject);


    VrulkBuffer *uniforms = new VrulkBuffer(physicalDevice, device, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, (VkDeviceSize) sizeof(UniformBufferObject), (uint32_t) swapchain->images.size());

    /** Textures
     */


    unsigned char* texture_data ;
    struct texture_data t_data;
    VkDeviceSize t_data_size;
    load_texture("textures/viking_room.png", &texture_data, &t_data_size, &t_data);
    uint32_t mips_level = static_cast<uint32_t>(std::floor(std::log2(std::max(t_data.width, t_data.height)))) + 1;


    VrulkTexture *texture = new VrulkTexture(physicalDevice, device, 
                                             &texture_data, t_data.width, t_data.height, t_data.channels, mips_level,
                                             commands->pool, &graphicsQueue);
    

    /** Model data **/
    VrulkMesh *model = new VrulkMesh("models/vulkan_room.obj", "textures/viking_room.png");

    VkDeviceSize vertex_data_size = sizeof(model->get_vertices()[0]) * model->get_vertices().size();
    printf("%lu\n", vertex_data_size);
    VrulkBuffer *model_vertices = new VrulkBuffer(physicalDevice, device, 
                                                  commands->pool, graphicsQueue,
                                                  VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, 
                                                  vertex_data_size, model->get_vertices().data(), 1);

    VrulkBuffer *model_indices = new VrulkBuffer(physicalDevice, device, commands->pool, graphicsQueue,
                                                 VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                                                 sizeof(model->get_indices()[0]) * model->get_indices().size(), model->get_indices().data(), 1);


    /** Descriptors **/
    std::vector<VkDescriptorBufferInfo> desc_buffers = create_buffer_descriptors(uniforms->buffers, (VkDeviceSize) sizeof(UniformBufferObject));
    VkDescriptorImageInfo desc_image = create_image_descriptor(texture->image_view, texture->get_sampler(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    bind_descriptor_sets(device, renderer->descriptor_sets, desc_buffers, 0, (VkDeviceSize) sizeof(UniformBufferObject), &desc_image, 1);

    
    VkResult status = cmd_Render_indices(commands->command_buffers, 
                                         renderer->get_render_pass(), 
                                         renderer->frame_buffers, 
                                         swapchain->get_extent(), 
                                         renderer->get_pipeline(), 
                                         renderer->get_pipeline_layout(),
                                         renderer->descriptor_sets,
                                         model->get_vertices(), 
                                         model->get_indices(), 
                                         &model_vertices->buffers[0], 
                                         &model_indices->buffers[0]);

    if (status != VK_SUCCESS) {
        exit(1);
    }
    VkSemaphoreCreateInfo semaphoreInfo;
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphoreInfo.pNext = VK_NULL_HANDLE;
    semaphoreInfo.flags = 0;

    VkFenceCreateInfo fenceInfo;
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.pNext = VK_NULL_HANDLE;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT; // initialize to signaled state. 


    std::vector<VkSemaphore> imageAvailableSemaphore;
    std::vector<VkSemaphore> renderFinishedSemaphore;
    std::vector<VkFence> inFlightFences;
    std::vector<VkFence> imagesInFlight;

    imageAvailableSemaphore.resize(MAX_FRAMES_IN_FLIGHT);
    renderFinishedSemaphore.resize(MAX_FRAMES_IN_FLIGHT);

    inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
    imagesInFlight.resize(swapchain->images.size(), VK_NULL_HANDLE);

    for(int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphore[i]) != VK_SUCCESS ||
            vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphore[i]) != VK_SUCCESS ||
            vkCreateFence(device, &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS) {
            print_error("Could not create semaphores");
        }
    }
    
        
    size_t current_frame = 0;
    // main loop
    // ---------
    while(!glfwWindowShouldClose(window)) {
        glfwPollEvents();
        drawFrame(physicalDevice, 
                  &device, 
                  &graphicsQueue, 
                  &presentQueue, 
                  &framebuffer_resized, 
                  &swapchain,
                  &renderer,
                  &commands,
                  &uniforms,
                  engine_tools, 
                  model,
                  model_vertices,
                  model_indices,
                  texture,
                  &descriptorSetLayout,
                  imageAvailableSemaphore, renderFinishedSemaphore, 
                  inFlightFences, imagesInFlight, &current_frame);
    }
    
    vkDeviceWaitIdle(device);
    




    // cleanup
    //

    
    for(int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(device, renderFinishedSemaphore[i], nullptr);
        vkDestroySemaphore(device, imageAvailableSemaphore[i], nullptr);
        vkDestroyFence(device, inFlightFences[i], nullptr);
    }

    texture->clean_up(device);

    commands->clean_up(device);
    renderer->clean_up(device);
    uniforms->clean_up(device);
    swapchain->clean_up(physicalDevice, device);
    model_vertices->clean_up(device);
    model_indices->clean_up(device);
    delete(texture);
    delete(commands);
    delete(renderer);
    delete(uniforms);
    delete(swapchain);

    vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);


    vkDestroyBuffer(device, index_buffer, nullptr);
    vkFreeMemory(device, *index_buffer_memory, nullptr);
    free(index_buffer_memory);
    

    
    vkDestroyShaderModule(device, fragmentModule, nullptr);
    vkDestroyShaderModule(device, vertexModule, nullptr);
    
    

    if (enableValidationLayers) {
        DestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr); 
    }
    // Queue's get destroyed when the logical device gets destroyed
    vkDestroyDevice(device, nullptr);
    

    vkDestroySurfaceKHR(instance, surface, nullptr);
    vkDestroyInstance(instance, nullptr);
    delete(engine_tools);
    

    /*
    glfwDestroyWindow(window);
    glfwTerminate();
    */

#if DEBUG
    fclose(fDebug);
#endif
}

void updateUniformBuffer(VkDevice device, uint32_t currentImage, std::vector<VkDeviceMemory> uniform_memory, float width, float height) {
    static auto startTime = std::chrono::high_resolution_clock::now();

    auto currentTime = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

    UniformBufferObject ubo = {};
    ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    ubo.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    ubo.proj = glm::perspective(glm::radians(45.0f), width / height, 0.1f, 10.0f);
    ubo.proj[1][1] *= -1;


    mapData(device, &uniform_memory[currentImage], (VkDeviceSize) sizeof(ubo), (const void *) (&ubo));
}

void recreate_engine(VkPhysicalDevice physicalDevice, 
                     VkDevice* device, 
                     VrulkSwapchain** swapchain,
                     VrulkPipeline** renderer,
                     VrulkCommands** commands,
                     VrulkBuffer** uniforms,
                     VrulkEngineTools* tools,
                     VrulkTexture* texture,
                     VkDescriptorSetLayout* descriptor_set_layout,
                     VrulkMesh* model,
                     VrulkBuffer* vertex_buffer,
                     VrulkBuffer* index_buffer) {
    int width = 0, height = 0;
    glfwGetFramebufferSize(*tools->window, &width, &height);
    while (width == 0 || height == 0) {
        glfwGetFramebufferSize(*tools->window, &width, &height);
        glfwWaitEvents();
    }

    vkDeviceWaitIdle(*device);

    (*commands)->clean_up(*device);
    (*renderer)->clean_up(*device);
    (*swapchain)->clean_up(physicalDevice, *device);
    (*uniforms)->clean_up(*device);

    delete(*commands);
    delete(*renderer);
    delete(*swapchain);
    delete(*uniforms);

    (*swapchain) = new VrulkSwapchain(physicalDevice, *device, tools->queue_set, tools->surface, tools->window);
    (*renderer) = new VrulkPipeline(physicalDevice, *device, **swapchain, descriptor_set_layout, tools->shader_stages.data(), tools->shader_amount, tools->queue_index, getMaxUseableCount(physicalDevice));
    (*commands) = new VrulkCommands(*device, tools->queue_index, (*renderer)->frame_buffers.size());

    (*uniforms) = new VrulkBuffer(physicalDevice, *device, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, (VkDeviceSize) sizeof(UniformBufferObject), (*swapchain)->images.size());

    std::vector<VkDescriptorBufferInfo> desc_buffers = create_buffer_descriptors((*uniforms)->buffers, (VkDeviceSize) sizeof(UniformBufferObject));
    VkDescriptorImageInfo desc_image = create_image_descriptor(texture->image_view, texture->get_sampler(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    bind_descriptor_sets(*device, (*renderer)->descriptor_sets, desc_buffers, 0, (VkDeviceSize) sizeof(UniformBufferObject), &desc_image, 1);



    VkResult status = cmd_Render_indices((*commands)->command_buffers, 
                                         (*renderer)->get_render_pass(), 
                                         (*renderer)->frame_buffers, 
                                         (*swapchain)->get_extent(), 
                                         (*renderer)->get_pipeline(), 
                                         (*renderer)->get_pipeline_layout(),
                                         (*renderer)->descriptor_sets,
                                         model->get_vertices(), 
                                         model->get_indices(), 
                                         &(vertex_buffer->buffers[0]), 
                                         &(index_buffer->buffers[0]));



}

void drawFrame(VkPhysicalDevice physicalDevice, VkDevice* device, 
               VkQueue* graphicsQueue, VkQueue* presentQueue, 
               bool *framebufferResized, 
               VrulkSwapchain** swapchain,
               VrulkPipeline** renderer,
               VrulkCommands** commands,
               VrulkBuffer** uniforms,
               VrulkEngineTools* tools,
               VrulkMesh* model,
               VrulkBuffer* vertex_buffer,
               VrulkBuffer* index_buffer,
               VrulkTexture* texture,
               VkDescriptorSetLayout* descriptor_set_layout,
               std::vector<VkSemaphore> imageSemaphore, std::vector<VkSemaphore> renderSemaphore,
               std::vector<VkFence> flightFences, std::vector<VkFence> imagesInFlight,
               size_t *currentFrame) 
{
    // wait for current frame to be available
    vkWaitForFences(*device, 1, &flightFences[*currentFrame], VK_TRUE, UINT64_MAX);


    // acquire image from swap chain
    uint32_t imageIndex;
    VkResult result =  vkAcquireNextImageKHR(*device, (*swapchain)->get_swapchain(), UINT32_MAX, imageSemaphore[*currentFrame], nullptr, &imageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        printf("RECREATING\n");
        recreate_engine(physicalDevice, 
                        device, 
                        swapchain, 
                        renderer, 
                        commands, 
                        uniforms,
                        tools, 
                        texture,
                        descriptor_set_layout,
                        model,
                        vertex_buffer,
                        index_buffer);
        return;
    } else if (result !=VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        exit(1);
    }

    // Check if a previous frame is using this image (i.e. there is its fence to wait on
    if (imagesInFlight[imageIndex] != VK_NULL_HANDLE) {
        vkWaitForFences(*device, 1, &imagesInFlight[imageIndex], VK_TRUE, UINT64_MAX);
    }

    // Mark the image as now being in use by this frame
    imagesInFlight[imageIndex] = flightFences[*currentFrame];

    updateUniformBuffer(*device, 
                        imageIndex, 
                        (*uniforms)->memory, 
                        (*swapchain)->get_extent().width, (*swapchain)->get_extent().height);

    VkSubmitInfo submitInfo;
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.pNext = VK_NULL_HANDLE;

    VkSemaphore waitSemaphores[] = {imageSemaphore[*currentFrame]};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;
    // specify command buffers to submit for execution
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &(*commands)->command_buffers[imageIndex];

    // signal semaphores once command buffer has finished executing
    VkSemaphore signalSemaphores[] = {renderSemaphore[*currentFrame]};
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    vkResetFences(*device, 1, &flightFences[*currentFrame]);

    if (vkQueueSubmit(*graphicsQueue, 1, &submitInfo, flightFences[*currentFrame]) != VK_SUCCESS) {
        print_error("Unable to submit queue");
        exit(1);
    }

    VkPresentInfoKHR presentInfo;
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.pNext = VK_NULL_HANDLE;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;
    VkSwapchainKHR swapChains[] = {(*swapchain)->get_swapchain()};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = nullptr;

    result = vkQueuePresentKHR(*presentQueue, &presentInfo);
    
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || *framebufferResized) {
        *framebufferResized = false;
        printf("Recreating!!!\n");
        recreate_engine(physicalDevice, 
                        device, 
                        swapchain, 
                        renderer, 
                        commands, 
                        uniforms,
                        tools, 
                        texture,
                        descriptor_set_layout,
                        model,
                        vertex_buffer,
                        index_buffer);
    } else if (result != VK_SUCCESS) {
        exit(1);
    }

    vkQueueWaitIdle(*presentQueue);

    *currentFrame = (*currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}



