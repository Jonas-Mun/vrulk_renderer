#ifndef VRULK_MODEL_H
#define VRULK_MODEL_H


#include <string>
#include <vector>
#include "vulkan_header.h"
#include "vrulk_engine.h"
#include "vertex.h"

class VrulkMesh
{
private:
    std::string texture_path;
    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;


    void load_model(std::string mod_path);
public:
    VrulkMesh(std::string model_path, std::string text_path);
    const std::vector<Vertex> get_vertices();
    const std::vector<uint32_t> get_indices();
    std::string get_texture_path();
};
#endif
