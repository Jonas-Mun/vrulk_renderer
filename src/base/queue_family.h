#ifndef QUEUE_FAMILY_H
#define QUEUE_FAMILY_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

/**
 * Stores the indices for a specific 
 * queue family in Vulkan
 *
 * Stack-based structure.
 * Does not use heap memory to create the structure
 */


struct QueueFamily {
    bool filled;
    uint32_t familyIndex;
};

void init_queue_family(struct QueueFamily* qf);
bool found_queue(struct QueueFamily *qf);


struct RequiredQueues {
    struct QueueFamily presentQueue;
    struct QueueFamily graphicQueue;
};

#endif
