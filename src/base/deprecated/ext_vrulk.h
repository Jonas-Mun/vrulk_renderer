#include "vulkan_header.h"
#include <stdio.h>
#include <vector>

std::vector<const char *> getRequiredExtensions(FILE** fInfo);
struct QueueFamily surfaceSupport(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface);

/*
 * Swap Chain
 */
VkSwapchainKHR createSwapchain(VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface, struct RequiredQueues* queues, GLFWwindow** window, FILE** fInfo);
