#include "ext_vrulk.h"
#include "queue_family.h"
#include <vector>

/******************
 * DEBUG ENUMS
 ******************/

const char* present_mode_names(VkPresentModeKHR present_enum) {
    switch(present_enum) {
        case 0:
            return "VK_PRESENT_MODE_IMMEDIATE_KHR";
        case 1:
            return "VK_PRESENT_MODE_MAILBOX_KHR";
        case 2:
            return "VK_PRESENT_MODE_FIFO_KHR";
        case 3:
            return "VK_PRESENT_MODE_FIFO_RELAXED_KHR";
        case 1000111000:
            return "VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR";
        case 1000111001:
            return "VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR";
        default:
            return "Invalid Present Mode";
    }
}

const char* format_mode_names(VkFormat format_enum) {
}

const char* color_space_mode_names( VkColorSpaceKHR color_space) {
}

/**************/

/***************
 * Helper FUnctions
 ***************/

uint32_t max(uint32_t x, uint32_t y) {
    if (x > y) {
        return x;
    }
    return y;
}

uint32_t min(uint32_t x, uint32_t y) {
    if (x < y) {
        return x;
    }
    return y;
}

/**************/

std::vector<const char *> getRequiredExtensions(FILE** fInfo) {
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char *> v_extension;

    // store glfw extensions
    for (int i = 0; i < glfwExtensionCount; i++) {
        v_extension.push_back(glfwExtensions[i]);
    }

    // store debug extensions
    v_extension.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

    return v_extension;
}

struct QueueFamily surfaceSupport(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface) {
    uint32_t queueCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueCount, nullptr); 
    VkQueueFamilyProperties* queueProperties = 
        (VkQueueFamilyProperties *) malloc(sizeof(VkQueueFamilyProperties) * queueCount);

    struct QueueFamily qFamily;
    init_queue_family(&qFamily);

    // Check for surface support
    // -------------------------
    for(uint32_t i = 0; i < queueCount; i++) {
        VkBool32 presentSupport = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &presentSupport);

        if (presentSupport) {
            qFamily.filled = true;
            qFamily.familyIndex = i;
            break;
        }
    }

    free(queueProperties);
    return qFamily;
}

/*********************
 * Swapchain support
 *********************/
VkSurfaceFormatKHR chooseSurfaceFormat(VkSurfaceFormatKHR formats[], int size) {
    for(int i = 0; i < size; i++) {
        if (formats[i].format == VK_FORMAT_B8G8R8A8_SRGB && formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            return formats[i];
        }
    }

    // default to firsr format
    return formats[0];
}

VkPresentModeKHR choosePresentMode(VkPresentModeKHR presents[], int size) {
    for(int i = 0; i < size; i++) {
        if (presents[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
            return presents[i];
        }
    }

    return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D chooseSwapExtent(VkSurfaceCapabilitiesKHR capabilities, GLFWwindow** window) {
    if (capabilities.currentExtent.width != UINT32_MAX) {
        return capabilities.currentExtent;
    } else {
        int width, height;
        glfwGetFramebufferSize(*window, &width, &height);

        VkExtent2D actualExtent = {
            static_cast<uint32_t>(width),
            static_cast<uint32_t>(height),
        };
        
        // clamp between minimum and maximum extens
        actualExtent.width = max(capabilities.minImageExtent.width, min(capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = max(capabilities.minImageExtent.height, min(capabilities.maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
                    
}

/** ---- **/

VkSwapchainKHR createSwapchain(VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface, struct RequiredQueues* queues, GLFWwindow** window, FILE** fInfo) {

    // supported formats
    // -----------------
    VkSurfaceFormatKHR* surfaceFormats;
    uint32_t formatCount = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, nullptr);
    surfaceFormats = (VkSurfaceFormatKHR *) malloc(sizeof(VkSurfaceFormatKHR) * formatCount);
    vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, surfaceFormats);
    
    /* Debug formats */
    if (fInfo != nullptr) {
        for(int i = 0; i < formatCount; i++) {
            VkSurfaceFormatKHR surfaceFormat = surfaceFormats[i];

            fprintf(*fInfo, "## Surface Formats Available ##\n");
            fprintf(*fInfo, "Format: %d\nColor Space: %d\n", surfaceFormat.format, surfaceFormat.colorSpace);
        }
    }
    /* ---- */

    // specify format
    
    // Surface capabilities
    // --------------------
    VkSurfaceCapabilitiesKHR surfaceCapabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities);

    /* Debug capabilities */
    if (fInfo != nullptr) {
        fprintf(*fInfo, "## Surface Capabilities ##\n");
        fprintf(*fInfo, "Min Image Count: %d\n Max Image Count: %d\n", surfaceCapabilities.minImageCount, surfaceCapabilities.maxImageCount);

        VkExtent2D currentExtent = surfaceCapabilities.currentExtent;
        fprintf(*fInfo, "-- Current Extent --\n");
        fprintf(*fInfo, "Width: %d\n Height: %d\n", currentExtent.width, currentExtent.height);

        VkExtent2D minExtent = surfaceCapabilities.minImageExtent;
        fprintf(*fInfo, "-- Min Image Extent --\n");
        fprintf(*fInfo, "Width: %d\n Height: %d\n", minExtent.width, minExtent.height);

        VkExtent2D maxExtent = surfaceCapabilities.maxImageExtent;
        fprintf(*fInfo, "-- Max Image Extent --\n");
        fprintf(*fInfo, "Width: %d\n Height: %d\n", maxExtent.width, maxExtent.height);

        fprintf(*fInfo, "Max Image Array Layers: %d\n", surfaceCapabilities.maxImageArrayLayers);
    }



    // Present Modes
    // -------------
    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, nullptr);
    VkPresentModeKHR* presentModes = (VkPresentModeKHR *) malloc(sizeof(VkPresentModeKHR) * presentModeCount);
    vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, presentModes);

    /* Debug */
    if (fInfo != nullptr) {
        for(int i = 0; i < presentModeCount; i++) {
            fprintf(*fInfo, "Present Mode: %s - %d\n", present_mode_names(presentModes[i]), presentModes[i]);
        }
    }
    /* -- */

    // Select info
    // -----------
    VkSurfaceFormatKHR surfaceFormat = chooseSurfaceFormat(surfaceFormats, formatCount);
    VkPresentModeKHR presentMode = choosePresentMode(presentModes, presentModeCount);
    VkExtent2D swapExtent = chooseSwapExtent(surfaceCapabilities, window);

    /* -- No longer needed --*/
    free(surfaceFormats);
    free(presentModes);


    // create info 
    VkSwapchainCreateInfoKHR createInfo;
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.pNext = VK_NULL_HANDLE;
    createInfo.surface = surface;
    createInfo.minImageCount = surfaceCapabilities.minImageCount;

    createInfo.flags = 0;

    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = swapExtent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    createInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    createInfo.presentMode = presentMode;

    createInfo.clipped = VK_TRUE;

    createInfo.oldSwapchain = VK_NULL_HANDLE;

    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    // --- Get Queues ---
    // Store queues in an array of usize_t

    
    if (queues->presentQueue.familyIndex == queues->graphicQueue.familyIndex) {
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0;
        createInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    } else {    // might be a memory error when using arrays
        uint32_t queueIndices[] = {queues->presentQueue.familyIndex, queues->graphicQueue.familyIndex};
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueIndices;
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
    }


    VkSwapchainKHR swapchain;
    if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapchain) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    } else {
        return swapchain;
    }
}

