#ifndef BASE_IMAGE_H
#define BASE_IMAGE_H
#include "vulkan_header.h"

#include <vector>

VkDescriptorImageInfo create_image_descriptor(VkImageView image_view, VkSampler sampler, VkImageLayout layout);


VkImageView createImageView(VkDevice device, VkImage image, VkFormat format, VkImageAspectFlags aspect_flags, uint32_t mips_level);

std::vector<VkImageView> createImageViews(VkDevice device, uint32_t count, std::vector<VkImage> images, VkFormat format, VkImageAspectFlags aspect_flags, uint32_t mips_level);

VkImage createImage(VkDevice device, int width, int height, uint32_t mips_level, 
                    VkFormat format, VkImageTiling tiling, VkSampleCountFlagBits sample_count,
                    VkImageUsageFlags usage, VkMemoryPropertyFlags propreties);

VkDeviceMemory* allocateImageMemory(VkPhysicalDevice physicalDevice, VkDevice device, VkImage image, VkMemoryPropertyFlags porperties, VkDeviceMemory* memory);

void bindImageMemory(VkDevice device, VkImage* image, VkDeviceMemory* memory);



#endif
