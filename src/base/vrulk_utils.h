#ifndef VRULK_UTILS_H
#define VRULK_UTILS_H
#include "vulkan_header.h"

uint32_t findMemoryType(VkPhysicalDevice physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties);

VkResult mapData(VkDevice device, VkDeviceMemory* buffer_memory, VkDeviceSize size, const void* g_data);

#endif
