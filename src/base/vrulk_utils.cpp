#include "vrulk_utils.h"
#include <cstring>

uint32_t findMemoryType(VkPhysicalDevice physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties) {
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
        if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }
    return 0;
}


VkResult mapData(VkDevice device, VkDeviceMemory* buffer_memory, VkDeviceSize size, const void* g_data) 
{     
    void* data;     
    vkMapMemory(device, *buffer_memory, 0, size, 0, &data);     
    memcpy(data, g_data, (size_t) size);     
    vkUnmapMemory(device, *buffer_memory);     
    return VK_SUCCESS; 
}
