#include "queue_family.h"

void init_queue_family(struct QueueFamily* qf) {
    qf->filled = false;
    qf->familyIndex = 0;
}

bool found_queue(struct QueueFamily *qf) {
    return qf->filled;
}
