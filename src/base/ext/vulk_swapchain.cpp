#include "vulk_swapchain.h"

VkSwapchainKHR createSwapchain(VkPhysicalDevice physicalDevice,
                               VkDevice device,
                               VkSurfaceKHR surface,
                               VkSurfaceFormatKHR surface_format,
                               VkPresentModeKHR present_mode,
                               VkExtent2D swap_extent,
                               uint32_t min_image,
                               std::set<uint32_t> queues,
                               GLFWwindow** window,
                               FILE** fInfo
        ) 
{
    VkSwapchainCreateInfoKHR createInfo;
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.pNext = VK_NULL_HANDLE;
    createInfo.surface = surface;
    createInfo.minImageCount = min_image;

    createInfo.flags = 0;

    createInfo.imageFormat = surface_format.format;
    createInfo.imageColorSpace = surface_format.colorSpace;
    createInfo.imageExtent = swap_extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    createInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    createInfo.presentMode = present_mode;

    createInfo.clipped = VK_TRUE;

    createInfo.oldSwapchain = VK_NULL_HANDLE;

    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    if (queues.size() > 0 ) {
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0;
        createInfo.pQueueFamilyIndices = VK_NULL_HANDLE;
    } else {
        // get queue indices from set of queues
        /* A */
        uint32_t* queueIndices = (uint32_t *) malloc(sizeof(uint32_t) * queues.size()); // I imagine the swapchain frees this memory
        std::set<uint32_t>::iterator itr;
        int i = 0;
        for(itr = queues.begin(); itr != queues.end(); itr++) {
            queueIndices[i] = *itr;
            i++;
        }

        createInfo.queueFamilyIndexCount = queues.size();
        createInfo.pQueueFamilyIndices = queueIndices;
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
    }

    VkSwapchainKHR swapchain;
    if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapchain) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    } else {
        return swapchain;
    }

}
