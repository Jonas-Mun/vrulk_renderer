#include "../vulkan_header.h"

VkSurfaceFormatKHR chooseSurfaceFormat(VkSurfaceFormatKHR formats[], uint32_t size);
VkExtent2D chooseSwapExtent(VkSurfaceCapabilitiesKHR capabilities, GLFWwindow** window);
VkPresentModeKHR choosePresentMode(VkPresentModeKHR presents[], int size);


struct QueueFamily surfaceSupport(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface);
VkSurfaceFormatKHR surfaceFormat(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface);
VkExtent2D surfaceExtent(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, GLFWwindow **window);
VkPresentModeKHR surfacePresentModes(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface);
uint32_t surfaceMinImageCount(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface);
