#include "ext.h"

std::vector<const char *> getInstanceExtensions(FILE** fInfo) {
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char *> v_extension;

    // store glfw extensions
    for (uint32_t i = 0; i < glfwExtensionCount; i++) {
        v_extension.push_back(glfwExtensions[i]);
    }

    // store debug extensions
    v_extension.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

    return v_extension;

}
