#include "vulk_surface.h"
#include "../queue_family.h"
#include <stdlib.h>

/******************
 * DEBUG ENUMS
 ******************/

const char* present_mode_names(VkPresentModeKHR present_enum) {
    switch(present_enum) {
        case 0:
            return "VK_PRESENT_MODE_IMMEDIATE_KHR";
        case 1:
            return "VK_PRESENT_MODE_MAILBOX_KHR";
        case 2:
            return "VK_PRESENT_MODE_FIFO_KHR";
        case 3:
            return "VK_PRESENT_MODE_FIFO_RELAXED_KHR";
        case 1000111000:
            return "VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR";
        case 1000111001:
            return "VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR";
        default:
            return "Invalid Present Mode";
    }
}


/**************/

/***************
 * Helper FUnctions
 ***************/

uint32_t max(uint32_t x, uint32_t y) {
    if (x > y) {
        return x;
    }
    return y;
}

uint32_t min(uint32_t x, uint32_t y) {
    if (x < y) {
        return x;
    }
    return y;
}

/**************/


struct QueueFamily surfaceSupport(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface) {
    uint32_t queueCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueCount, nullptr); 
    VkQueueFamilyProperties* queueProperties = 
        (VkQueueFamilyProperties *) malloc(sizeof(VkQueueFamilyProperties) * queueCount);

    struct QueueFamily qFamily;
    init_queue_family(&qFamily);

    // Check for surface support
    // -------------------------
    for(uint32_t i = 0; i < queueCount; i++) {
        VkBool32 presentSupport = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &presentSupport);

        if (presentSupport) {
            qFamily.filled = true;
            qFamily.familyIndex = i;
            break;
        }
    }

    free(queueProperties);
    return qFamily;

}

VkSurfaceFormatKHR chooseSurfaceFormat(VkSurfaceFormatKHR formats[], uint32_t size) {
    for(uint32_t i = 0; i < size; i++) {
        if (formats[i].format == VK_FORMAT_B8G8R8A8_SRGB && formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            return formats[i];
        }
    }

    // default to firsr format
    return formats[0];

}

VkPresentModeKHR choosePresentMode(VkPresentModeKHR presents[], int size) {
    for(int i = 0; i < size; i++) {
        if (presents[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
            return presents[i];
        }
    }

    return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D chooseSwapExtent(VkSurfaceCapabilitiesKHR capabilities, GLFWwindow** window) {
    if (capabilities.currentExtent.width != UINT32_MAX) {
        return capabilities.currentExtent;
    } else {
        int width, height;
        glfwGetFramebufferSize(*window, &width, &height);

        VkExtent2D actualExtent = {
            static_cast<uint32_t>(width),
            static_cast<uint32_t>(height),
        };
        
        // clamp between minimum and maximum extens
        actualExtent.width = max(capabilities.minImageExtent.width, min(capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = max(capabilities.minImageExtent.height, min(capabilities.maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
                    
}

/** Main Functions **/

VkSurfaceFormatKHR surfaceFormat(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface) {
    VkSurfaceFormatKHR* surfaceFormats;
    uint32_t formatCount;
    // acquire surface formats
    vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, nullptr);
    surfaceFormats = (VkSurfaceFormatKHR *) malloc(sizeof(VkSurfaceFormatKHR) * formatCount);
    vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, surfaceFormats);

    VkSurfaceFormatKHR format = chooseSurfaceFormat(surfaceFormats, formatCount);

    free(surfaceFormats);
    return format;
}

VkExtent2D surfaceExtent(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, GLFWwindow** window) {
    VkSurfaceCapabilitiesKHR capabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &capabilities);

    VkExtent2D extent = chooseSwapExtent(capabilities, window);

    return extent;
}

VkPresentModeKHR surfacePresentModes(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface) {
    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, nullptr);
    VkPresentModeKHR* presentModes = (VkPresentModeKHR *) malloc(sizeof(VkPresentModeKHR) * presentModeCount);

    vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, presentModes);

    VkPresentModeKHR presentMode = choosePresentMode(presentModes, presentModeCount);

    free(presentModes);
    return presentMode;
}

uint32_t surfaceMinImageCount(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface) {
    VkSurfaceCapabilitiesKHR capabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &capabilities);
    uint32_t image_count = capabilities.minImageCount + 1;
    if (capabilities.maxImageCount > 0 && image_count > capabilities.maxImageCount) {
        image_count = capabilities.maxImageCount;
    }

    return image_count;
}
