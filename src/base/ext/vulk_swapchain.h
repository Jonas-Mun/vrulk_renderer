#include "../vulkan_header.h"
#include <set>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>

VkSwapchainKHR createSwapchain(VkPhysicalDevice physicalDevice, 
                                VkDevice device, 
                                VkSurfaceKHR surface,
                                VkSurfaceFormatKHR surface_format, 
                                VkPresentModeKHR present_mode,
                                VkExtent2D swap_extent,
                                uint32_t min_image,
                                std::set<uint32_t> queues, 
                                GLFWwindow** window, 
                                FILE** fInfo
                                );
