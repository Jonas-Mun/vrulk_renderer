#include "base_image.h"
#include "vrulk_utils.h"

VkImageView createImageView(VkDevice device, VkImage image, VkFormat format, VkImageAspectFlags aspect_flags, uint32_t mips_level) {
    VkImageViewCreateInfo viewInfo;
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.pNext = VK_NULL_HANDLE;
    viewInfo.flags = 0;
    viewInfo.image = image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = format;
    viewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    viewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    viewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    viewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    viewInfo.subresourceRange.aspectMask = aspect_flags;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = mips_level;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;

    VkImageView image_view;

    if (vkCreateImageView(device, &viewInfo, nullptr, &image_view) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    } else {
        return image_view;
    }

}

std::vector<VkImageView> createImageViews(VkDevice device, uint32_t count, std::vector<VkImage> images, VkFormat format, VkImageAspectFlags aspect_flags, uint32_t mips_level) {
    // setup 
    std::vector<VkImageView> imageViews;
    imageViews.resize(count);

    // create images
    for(uint32_t i = 0; i < count; i++) {
        imageViews[i] = createImageView(device, images[i], format, aspect_flags, mips_level);
    }

    return imageViews;
}

VkImage createImage(VkDevice device, int width, int height, uint32_t mips_level,
                    VkFormat format, VkImageTiling tiling, VkSampleCountFlagBits sample_count,
                    VkImageUsageFlags usage, VkMemoryPropertyFlags propreties) {
    VkImage textureImage;

    VkImageCreateInfo imageInfo;
    imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageInfo.pNext = VK_NULL_HANDLE;
    imageInfo.flags = 0;
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.format = format; // same as in the texture buffer
    imageInfo.extent.width = width;
    imageInfo.extent.height = height;
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = mips_level;
    imageInfo.arrayLayers = 1;
    imageInfo.samples = sample_count;
    imageInfo.tiling = tiling;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageInfo.usage = usage;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.queueFamilyIndexCount = 0;
    imageInfo.pQueueFamilyIndices = nullptr;

    // Verify that the format is supported when preventing errors
    if (vkCreateImage(device, &imageInfo, nullptr, &textureImage) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    } else {
        return textureImage;
    }
}


VkDescriptorImageInfo create_image_descriptor(VkImageView image_view, VkSampler sampler, VkImageLayout layout) {
    VkDescriptorImageInfo info;
    info.sampler = sampler;
    info.imageView = image_view;
    info.imageLayout = layout;

    return info;
}

VkDeviceMemory* allocateImageMemory(VkPhysicalDevice physicalDevice, VkDevice device, VkImage image, VkMemoryPropertyFlags porperties, VkDeviceMemory* memory) {
    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(device, image, &memRequirements);

    VkMemoryAllocateInfo allocInfo;
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.pNext = VK_NULL_HANDLE;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = findMemoryType(physicalDevice, memRequirements.memoryTypeBits, porperties);

    if (vkAllocateMemory(device, &allocInfo, nullptr, memory) != VK_SUCCESS) {
        memory = VK_NULL_HANDLE;
        return VK_NULL_HANDLE;
    } else {
        return memory;
    }

}

void bindImageMemory(VkDevice device, VkImage* image, VkDeviceMemory* memory) {
    vkBindImageMemory(device, *image, *memory, 0);

}
