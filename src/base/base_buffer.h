#ifndef BASE_BUFFER_H
#define BASE_BUFFER_H
#include "vulkan_header.h"

#include <vector>

std::vector<VkDescriptorBufferInfo> create_buffer_descriptors(std::vector<VkBuffer> buffers, VkDeviceSize data_size);


VkBuffer createBuffer(VkDevice device, VkDeviceSize size, VkBufferUsageFlags usage);

/* Utilizes a staging buffer */
VkBuffer createVertexBuffer(VkDevice device, VkDeviceSize size, VkBufferUsageFlags flags);

VkBuffer createIndexBuffer(VkDevice device , VkDeviceSize size, VkBufferUsageFlags flags);

VkResult bindBuffer(VkDevice device, uint32_t size, VkBuffer* buffer, VkDeviceMemory* bufferMemory);

VkDeviceMemory* allocateBufferMemory(VkPhysicalDevice physicalDevice, VkDevice device, VkMemoryPropertyFlags memory_property, VkBuffer* buffer, VkDeviceMemory* memory);

void copyBuffer(VkDevice* device, 
                VkCommandPool commandPool, 
                VkQueue* queue, 
                VkBuffer* src_buffer, 
                VkBuffer* dstBuffer, 
                VkDeviceSize size);

bool create_mapped_buffer(VkPhysicalDevice physicalDevice, VkDevice device, 
                          VkCommandPool pool, VkQueue* queue,
                          VkDeviceSize size, 
                          VkBufferUsageFlags usage_flags, VkBuffer* buffer, 
                          VkDeviceMemory* buffer_memory, const void* g_data);

#endif
