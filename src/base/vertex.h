#ifndef VERTEX_H
#define VERTEX_H
#include <glm/glm.hpp>
#include "vulkan_header.h"
#include <vector>

#include <glm/gtx/hash.hpp>


struct Vertex {
    glm::vec3 pos;
    glm::vec3 color;
    glm::vec2 texCoord;


    bool operator==(const Vertex& other) const {
        return pos == other.pos && color == other.color && texCoord == other.texCoord;
    }

};

namespace std {
    template<> struct hash<Vertex> {
        size_t operator()(Vertex const& vertex) const {
            return ((hash<glm::vec3>()(vertex.pos) ^
                   (hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
                   (hash<glm::vec2>()(vertex.texCoord) << 1);
        }
    };
}

VkVertexInputBindingDescription vertex_binding_description();

std::vector<VkVertexInputAttributeDescription> vertex_attribute_description();



#endif
