#include "base_vrulk.h"
#include "queue_family.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#define P_CHECK(p) (assert(p != NULL))



static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData) {
    printf("validation layer: %s\n", pCallbackData->pMessage);
    return VK_FALSE;
}

VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
    PFN_vkCreateDebugUtilsMessengerEXT func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
    if (func != nullptr) {
        return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
    } else {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}



VkDebugUtilsMessengerEXT setupDebugMessenger(VkInstance instance, FILE** fInfo) {
    /* A */
    VkDebugUtilsMessengerCreateInfoEXT* createInfo = (VkDebugUtilsMessengerCreateInfoEXT *) malloc(sizeof(VkDebugUtilsMessengerCreateInfoEXT));
    
    populateDebugMessengerCreateInfo(createInfo);
    
    createInfo->pUserData = nullptr;
    
    VkDebugUtilsMessengerEXT debugMessenger;

    VkResult status = CreateDebugUtilsMessengerEXT(instance, createInfo, nullptr, &debugMessenger);

    /* ~A */
    free(createInfo);

    if (status != VK_SUCCESS) {
        fprintf(*fInfo,"Failed to create DebugUtl: %d\n", status);
        return VK_NULL_HANDLE;
    } else {
        return debugMessenger;
    }
    
}

void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT* createInfo) {
    //createInfo = {};  // causes segmentation fault 

    createInfo->sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo->messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                                 VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                 VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo->messageType     = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                                 VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                                 VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo->pfnUserCallback = debugCallback;
    createInfo->flags = 0;  // required for vulkan 1.2

}


bool graphicsCompatible(VkPhysicalDevice physicalDevice, VkPhysicalDeviceProperties properties) {
    if (properties.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
        return false;
    }

    // Check Queue requirements
    // ------------------------
    struct QueueFamily queueFamily = findQueue(physicalDevice, VK_QUEUE_GRAPHICS_BIT);

    // Check device feautures
    // ----------------------
    VkPhysicalDeviceFeatures supportedFeatures;
    vkGetPhysicalDeviceFeatures(physicalDevice, &supportedFeatures);


    return queueFamily.filled && supportedFeatures.samplerAnisotropy;

    
}

struct QueueFamily findQueue(VkPhysicalDevice physicalDevice, VkQueueFlagBits flagBit) {

    // Get Queues available
    // --------------------
    uint32_t queueCount;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueCount, nullptr);
    /* A */
    VkQueueFamilyProperties *queueProperties = 
        (VkQueueFamilyProperties *) malloc(sizeof(VkQueueFamilyProperties) * queueCount);

    P_CHECK(queueProperties);

    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueCount, queueProperties);

    struct QueueFamily queueFamily;
    init_queue_family(&queueFamily);
    // Find specific queue
    // -------------------
    for (uint32_t i = 0; i < queueCount; i++) {
        if (queueProperties[i].queueFlags & flagBit) {
            queueFamily.familyIndex = i;
            queueFamily.filled = true;
            break;
        }
    }

    /* ~A */
    free(queueProperties);

    return queueFamily;
}

bool checkValidationSupport(const char** layers, int layer_size) {
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

    VkLayerProperties* availableLayers = nullptr;
    /* A */
    availableLayers = (VkLayerProperties *) malloc(sizeof(VkLayerProperties) * layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers);

    // Go over desired layers

    bool layerFound = false;
    for (int i = 0; i < layer_size; i++) {
        const char* layerName = layers[i];
        // Go through all available layers
        for (uint32_t j = 0; j < layerCount; j++) {
            if (strcmp(layerName, availableLayers[j].layerName) == 0) {
                layerFound = true;
                break;
            }
        }

        if (layerFound) {
            break;
        }
    }
    /* A */
    free(availableLayers);

    return layerFound;
}

VkInstance createInstance(const char* app_name, uint32_t app_version,
                          const char* engine_name, uint32_t engine_version,
                          std::vector<const char *> instance_extensions,
                          bool validation_layers, FILE** fInfo) 
{
    const char* debugLayers[] = {
        "VK_LAYER_KHRONOS_validation"
    };

    if (validation_layers && !checkValidationSupport(debugLayers, (sizeof(debugLayers) / sizeof(debugLayers[0])))) {
        printf("Validation layers not supported\n");
        return VK_NULL_HANDLE;
    }

    VkApplicationInfo appInfo { VK_STRUCTURE_TYPE_APPLICATION_INFO };
    appInfo.pApplicationName = app_name;
    appInfo.applicationVersion = VK_MAKE_VERSION(app_version, 0, 0);
    appInfo.pEngineName = engine_name;
    appInfo.engineVersion = VK_MAKE_VERSION(engine_version, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_2;

    VkInstanceCreateInfo createInfo{ VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO };
    createInfo.pApplicationInfo = &appInfo;

    /* A */
    VkDebugUtilsMessengerCreateInfoEXT* debugCreateInfo = (VkDebugUtilsMessengerCreateInfoEXT* ) malloc(sizeof( VkDebugUtilsMessengerCreateInfoEXT));

    if (validation_layers) {
        createInfo.enabledLayerCount = sizeof(debugLayers) / sizeof(debugLayers[0]);
        createInfo.ppEnabledLayerNames = debugLayers;
        
        // extensions 
        createInfo.enabledExtensionCount = instance_extensions.size();
        createInfo.ppEnabledExtensionNames = instance_extensions.data();


        populateDebugMessengerCreateInfo(debugCreateInfo);
        createInfo.pNext = debugCreateInfo;
        
    } else {
        createInfo.pNext = VK_NULL_HANDLE;
    }

    VkInstance instance;
    if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS) {
        /* ~A */
        free(debugCreateInfo);
        return VK_NULL_HANDLE;
    }
    /* ~A */
    free(debugCreateInfo);
    return instance;

}

VkPhysicalDevice createPhysicalDevice(VkInstance instance, FILE** fInfo) {
    uint32_t deviceCount;
    // Get number of devices
    vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
    // Get all physical devices
    /* A */
    VkPhysicalDevice* physicalDevices = (VkPhysicalDevice *) malloc(sizeof(VkPhysicalDevice) * deviceCount);
    vkEnumeratePhysicalDevices(instance, &deviceCount, physicalDevices);

    VkPhysicalDeviceProperties properties;
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;

    // See properties
    for(uint32_t i = 0; i < deviceCount; i++) {
        vkGetPhysicalDeviceProperties(
                physicalDevices[i],
                &properties
            );
        if (fInfo != nullptr) {
            fprintf(*fInfo, "## Device info for: %s ##\n", properties.deviceName);
            fprintf(*fInfo, "API Version: %d\n", properties.apiVersion);
            fprintf(*fInfo, "Driver Version: %d\n", properties.driverVersion);
            fprintf(*fInfo, "Vendor ID: %d\n", properties.vendorID);
        }
        
        if (graphicsCompatible(physicalDevices[i], properties)) {

            physicalDevice = physicalDevices[i];
            break;
        }
    }

    /* ~A */
    free(physicalDevices);
    return physicalDevice;

}

std::vector<VkDeviceQueueCreateInfo> set_of_queue_info(std::vector<struct QueueFamily> queues) {
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    // Create the set
    for(uint32_t i = 0; i < queues.size(); i++) {

        bool already_created = false;
        float queuePriority = 1.0f;
        
        // Search if queue index already in set
        for(uint32_t q_j = 0; q_j < queueCreateInfos.size(); q_j++) {
            if (queueCreateInfos.at(q_j).queueFamilyIndex == queues.at(i).familyIndex) {
                already_created = true;
                break;
            }
            
        }
        // Create queue info for new queue index
        if (already_created == false) {
            VkDeviceQueueCreateInfo queueInfo;
            queueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueInfo.pNext = nullptr;
            queueInfo.flags = 0;
            queueInfo.queueFamilyIndex = queues.at(i).familyIndex;
            queueInfo.queueCount = 1;
            queueInfo.pQueuePriorities = &queuePriority;

            queueCreateInfos.push_back(queueInfo);
        }
    }

    return queueCreateInfos;
}

VkDevice createLogicalDevice(VkPhysicalDevice physicalDevice, std::vector<struct QueueFamily> extension_queues, std::vector<const char *> device_extensions, FILE** fInfo) {
    // acquire queue information to be created

    struct QueueFamily queueFamily = findQueue(physicalDevice, VK_QUEUE_GRAPHICS_BIT);
    if (found_queue(&queueFamily) == false) {
        return VK_NULL_HANDLE;
    }

    // add graphics queue
    extension_queues.push_back(queueFamily);

    // Avoid duplicate queue indices (Error would occur)
    std::vector<VkDeviceQueueCreateInfo> queueInfos = set_of_queue_info(extension_queues);

    VkPhysicalDeviceFeatures deviceFeatures {};
    deviceFeatures.samplerAnisotropy = VK_TRUE;
    //vkGetPhysicalDeviceFeatures(physicalDevice, &deviceFeatures);
    

    VkDeviceCreateInfo createInfo{VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO};
    createInfo.queueCreateInfoCount = queueInfos.size();
    createInfo.pQueueCreateInfos = queueInfos.data();
    createInfo.pEnabledFeatures = &deviceFeatures;

    // Provide extensions for swapchain. Otherwise a seg fault will occur
    //const char ** extensionNames = (const char **) malloc(sizeof(char *) * 1);
    //*extensionNames = VK_KHR_SWAPCHAIN_EXTENSION_NAME;
    createInfo.enabledExtensionCount = device_extensions.size();
    createInfo.ppEnabledExtensionNames = device_extensions.data(); 

    VkDevice device;
    if (vkCreateDevice(physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    }
return device;
}

VkShaderModule createShaderModule(VkDevice device, std::vector<char> code) {
    // shader create info
    VkShaderModuleCreateInfo createInfo;
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.pNext = VK_NULL_HANDLE;
    createInfo.flags = 0;
    createInfo.codeSize = code.size();
    createInfo.pCode = (const uint32_t *)code.data();
   
    VkShaderModule module;
    vkCreateShaderModule(device, &createInfo, nullptr, &module);

    return module;
}

VkPipelineShaderStageCreateInfo createShaderStage(VkDevice device, VkShaderModule module, const char * entry, VkShaderStageFlagBits flag_bit) {
    VkPipelineShaderStageCreateInfo createInfo;
    createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    createInfo.pNext = VK_NULL_HANDLE;
    createInfo.flags = 0;
    createInfo.stage = flag_bit;
    createInfo.module = module;     // don't forget to include the shader module
    createInfo.pName = entry;
    createInfo.pSpecializationInfo = VK_NULL_HANDLE;

    return createInfo;
}

VkPipeline createGraphicsPipeline(VkDevice device, 
                                  VkRenderPass render_pass, 
                                  VkPipelineShaderStageCreateInfo* shaders, 
                                  VkPipelineLayout pipeline_layout, VkExtent2D extent,
                                  VkVertexInputBindingDescription* binding_description, 
                                  VkSampleCountFlagBits msaa_samples,
                                  bool depth,
                                  std::vector<VkVertexInputAttributeDescription> attribute_description) 
{
    VkPipelineVertexInputStateCreateInfo vertexInputState;
    vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputState.pNext = VK_NULL_HANDLE;
    vertexInputState.flags = 0;
    if (binding_description == nullptr || attribute_description.size() == 0 ) {
        vertexInputState.vertexBindingDescriptionCount = 0;
        vertexInputState.pVertexBindingDescriptions = VK_NULL_HANDLE;
        vertexInputState.vertexAttributeDescriptionCount = 0;
        vertexInputState.pVertexAttributeDescriptions = VK_NULL_HANDLE;
    } else {
        vertexInputState.vertexBindingDescriptionCount = 1;
        vertexInputState.pVertexBindingDescriptions = binding_description;
        vertexInputState.vertexAttributeDescriptionCount = attribute_description.size();
        vertexInputState.pVertexAttributeDescriptions = attribute_description.data();
    }
    
    // # Assembly input state
    VkPipelineInputAssemblyStateCreateInfo assemblyInputState;
    assemblyInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    assemblyInputState.pNext = VK_NULL_HANDLE;
    assemblyInputState.flags = 0;
    assemblyInputState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    assemblyInputState.primitiveRestartEnable = VK_FALSE;

    // Viewport & Scissors

    VkViewport viewport;
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = extent.width;
    viewport.height = extent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor;
    scissor.offset = VkOffset2D{0, 0};
    scissor.extent = extent;

    VkPipelineViewportStateCreateInfo viewportState;
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.pNext = VK_NULL_HANDLE;
    viewportState.flags = 0;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    // Rasterization state
    VkPipelineRasterizationStateCreateInfo rasterInfo;
    rasterInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterInfo.pNext = VK_NULL_HANDLE;
    rasterInfo.flags = 0;
    rasterInfo.depthClampEnable = VK_FALSE;
    rasterInfo.rasterizerDiscardEnable = VK_FALSE;
    rasterInfo.polygonMode = VK_POLYGON_MODE_FILL;
    rasterInfo.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterInfo.depthBiasEnable = VK_FALSE;
    rasterInfo.depthBiasConstantFactor = 0.0f;
    rasterInfo.depthBiasClamp = 0.0f;
    rasterInfo.depthBiasSlopeFactor = 0.0f;
    rasterInfo.lineWidth = 1.0f;

    VkPipelineMultisampleStateCreateInfo mulsamInfo;
    mulsamInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    mulsamInfo.pNext = VK_NULL_HANDLE;
    mulsamInfo.flags = 0;
    mulsamInfo.rasterizationSamples = msaa_samples;
    mulsamInfo.sampleShadingEnable = VK_FALSE;
    mulsamInfo.minSampleShading = 0.0f;
    mulsamInfo.pSampleMask = nullptr;
    mulsamInfo.alphaToCoverageEnable = VK_FALSE;
    mulsamInfo.alphaToOneEnable = VK_FALSE;

    VkPipelineDepthStencilStateCreateInfo depthInfo;
    if (depth) {
        depthInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthInfo.pNext = VK_NULL_HANDLE;
        depthInfo.flags = 0;
        depthInfo.depthTestEnable = VK_TRUE;
        depthInfo.depthWriteEnable = VK_TRUE;
        depthInfo.depthCompareOp = VK_COMPARE_OP_LESS;
        depthInfo.depthBoundsTestEnable = VK_FALSE;
        depthInfo.stencilTestEnable = VK_FALSE;
        depthInfo.front = {};
        depthInfo.back = {};
        depthInfo.minDepthBounds = 0.0f;
        depthInfo.maxDepthBounds = 1.0f;
    }
    // blend color from framebuffer with new fragment color
    
    // Configuration per framebuffer
    VkPipelineColorBlendAttachmentState colorAttachment;
    colorAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorAttachment.blendEnable = VK_FALSE;
    colorAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    colorAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorAttachment.colorBlendOp = VK_BLEND_OP_ADD;
    colorAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorAttachment.alphaBlendOp = VK_BLEND_OP_ADD;


    // global color blending
    VkPipelineColorBlendStateCreateInfo blendInfo;
    blendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    blendInfo.pNext = VK_NULL_HANDLE;
    blendInfo.flags = 0;
    blendInfo.logicOpEnable = VK_FALSE;
    blendInfo.logicOp  = VK_LOGIC_OP_COPY;
    blendInfo.attachmentCount = 1;
    blendInfo.pAttachments = &colorAttachment;
    // fill color blends
    for(int i = 0; i < 4; i++) {
        blendInfo.blendConstants[i] = 0.0f;
    }

    
    /*
    VkDynamicState dynamicStates[] = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_LINE_WIDTH
    };
    */

    VkPipelineDynamicStateCreateInfo dynInfo;
    dynInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynInfo.pNext = VK_NULL_HANDLE;
    dynInfo.flags = 0;
    dynInfo.dynamicStateCount = 0;
    dynInfo.pDynamicStates = nullptr;

    VkGraphicsPipelineCreateInfo graphicsInfo;
    graphicsInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    graphicsInfo.pNext = VK_NULL_HANDLE;
    graphicsInfo.flags = 0;
    graphicsInfo.stageCount = 2;
    graphicsInfo.pStages = shaders;
    graphicsInfo.pVertexInputState = &vertexInputState;
    graphicsInfo.pInputAssemblyState = &assemblyInputState;
    graphicsInfo.pTessellationState = nullptr;
    graphicsInfo.pViewportState = &viewportState;
    graphicsInfo.pRasterizationState = &rasterInfo;
    graphicsInfo.pMultisampleState = &mulsamInfo;
    if (depth) {
        graphicsInfo.pDepthStencilState = &depthInfo;
    } else {
        graphicsInfo.pDepthStencilState = VK_NULL_HANDLE;
    }
    graphicsInfo.pColorBlendState = &blendInfo;
    graphicsInfo.pDynamicState = &dynInfo;
    graphicsInfo.layout = pipeline_layout;
    graphicsInfo.renderPass = render_pass;
    graphicsInfo.subpass = 0;   // index of subpass
    graphicsInfo.basePipelineHandle = VK_NULL_HANDLE;
    graphicsInfo.basePipelineIndex = -1;

    VkPipeline graphics_pipeline;
    if (vkCreateGraphicsPipelines(device, nullptr, 1, &graphicsInfo, nullptr, &graphics_pipeline) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    }

    return graphics_pipeline;

}

/* For some reason when returning a VkSubpass description, the layout value also changes */
/*
VkSubpassDescription createSubpassDescription(std::vector<VkAttachmentReference> color, VkAttachmentReference *depth) {


    VkSubpassDescription subpassDesc;
    subpassDesc.flags = 0;
    subpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDesc.inputAttachmentCount = 0;
    subpassDesc.pInputAttachments = nullptr;
    // referenced from fragment shader
    subpassDesc.colorAttachmentCount = color.size();
    subpassDesc.pColorAttachments = color.data();
    printf("%u\n", subpassDesc.pColorAttachments[0].layout);
    // ---
    subpassDesc.pResolveAttachments = nullptr;
    subpassDesc.pDepthStencilAttachment = depth;
    subpassDesc.preserveAttachmentCount = 0;
    subpassDesc.pPreserveAttachments = nullptr;
    printf("%u\n", subpassDesc.pColorAttachments[0].layout);

    return subpassDesc;

}
*/

VkAttachmentDescription createAttachmentDescription(VkFormat format, VkImageLayout final_layout, VkAttachmentStoreOp store_op, VkAttachmentLoadOp load_op, VkSampleCountFlagBits msaa_samples) {
    VkAttachmentDescription att;
    att.flags = 0;
    att.format = format;
    att.samples = msaa_samples;
    att.loadOp = load_op;   // clear framebuffer to black before drawing
    att.storeOp = store_op; // store rendered contents
    att.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    att.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    att.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    att.finalLayout = final_layout;  // present to swap chain

    return att;
}

VkAttachmentReference createAttachmentReference(uint32_t index, VkImageLayout layout) {
    VkAttachmentReference refInfo;
    refInfo.attachment = index;
    refInfo.layout = layout;


    return refInfo;
}

VkSubpassDependency createSubpassDependency(uint32_t src_sub, uint32_t dst_sub, 
                                            VkPipelineStageFlags src_stage_mask, VkPipelineStageFlags dst_stage_mask,
                                            VkAccessFlags src_access_mask, VkAccessFlags dst_access_mask, VkDependencyFlags dependencies) 
{
    VkSubpassDependency dependency;
    dependency.srcSubpass = src_sub;    //implicit subpass before the render pass
    dependency.dstSubpass = dst_sub;  // refer to our subpass
    dependency.srcStageMask = src_stage_mask;
    dependency.srcAccessMask = src_access_mask;
    dependency.dstStageMask = dst_stage_mask;
    dependency.dstAccessMask = dst_access_mask;
    dependency.dependencyFlags = dependencies;

    return dependency;
}

VkRenderPass createRenderPass(VkDevice device, std::vector<VkAttachmentDescription> attach_desc, std::vector<VkSubpassDescription> subpass_desc, std::vector<VkSubpassDependency> subpass_dependency) {
    VkRenderPassCreateInfo renderInfo;
    renderInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderInfo.pNext = VK_NULL_HANDLE;
    renderInfo.flags = 0;
    renderInfo.attachmentCount = attach_desc.size();
    renderInfo.pAttachments = attach_desc.data();
    renderInfo.subpassCount = subpass_desc.size();
    renderInfo.pSubpasses = subpass_desc.data();
    renderInfo.dependencyCount = subpass_dependency.size();
    renderInfo.pDependencies = subpass_dependency.data();

    VkRenderPass render_pass;
    if (vkCreateRenderPass(device, &renderInfo, nullptr, &render_pass) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    }

    return render_pass;
}

VkRenderPass createRenderPass_Color(VkDevice device, VkFormat format) {

    /* Prepare Render Pass */
    std::vector<VkAttachmentDescription> attachDesc;
    attachDesc.resize(1);
    attachDesc[0] = createAttachmentDescription(format, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, VK_ATTACHMENT_STORE_OP_STORE, VK_ATTACHMENT_LOAD_OP_CLEAR,VK_SAMPLE_COUNT_1_BIT);

    std::vector<VkAttachmentReference> colorRefs;
    colorRefs.resize(1);
    colorRefs[0] = createAttachmentReference(0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);


    std::vector<VkSubpassDescription> subpassDesc;
    subpassDesc.resize(1);
    subpassDesc[0].flags = 0;
    subpassDesc[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDesc[0].inputAttachmentCount = 0;
    subpassDesc[0].pInputAttachments = nullptr;
    // referenced from fragment shader
    subpassDesc[0].colorAttachmentCount = colorRefs.size();
    subpassDesc[0].pColorAttachments = colorRefs.data();
    // ---
    subpassDesc[0].pResolveAttachments = nullptr;
    subpassDesc[0].pDepthStencilAttachment = nullptr;
    subpassDesc[0].preserveAttachmentCount = 0;
    subpassDesc[0].pPreserveAttachments = nullptr;

    std::vector<VkSubpassDependency> dependency;
    dependency.resize(1);
    dependency[0] = createSubpassDependency(VK_SUBPASS_EXTERNAL, 0, 
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT ,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT ,
            0, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT , 
            0);
    return createRenderPass(device, attachDesc, subpassDesc, dependency);
}

VkRenderPass createRenderPass_ColorDepth(VkPhysicalDevice physicalDevice, VkDevice device, VkFormat format){
    std::vector<VkAttachmentDescription> attachDesc;
    attachDesc.resize(2);
    attachDesc[0] = createAttachmentDescription(format, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, VK_ATTACHMENT_STORE_OP_STORE, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_SAMPLE_COUNT_1_BIT);
    attachDesc[1] = createAttachmentDescription(findDepthFormat(physicalDevice), VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, VK_ATTACHMENT_STORE_OP_DONT_CARE, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_SAMPLE_COUNT_1_BIT);

    std::vector<VkAttachmentReference> colorRefs;
    colorRefs.resize(1);
    colorRefs[0] = createAttachmentReference(0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

    VkAttachmentReference depthRef;
    depthRef = createAttachmentReference(1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

    std::vector<VkSubpassDescription> subpassDesc;
    subpassDesc.resize(1);
    subpassDesc[0].flags = 0;
    subpassDesc[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDesc[0].inputAttachmentCount = 0;
    subpassDesc[0].pInputAttachments = nullptr;
    // referenced from fragment shader
    subpassDesc[0].colorAttachmentCount = colorRefs.size();
    subpassDesc[0].pColorAttachments = colorRefs.data();
    // ---
    subpassDesc[0].pResolveAttachments = nullptr;
    subpassDesc[0].pDepthStencilAttachment = &depthRef;
    subpassDesc[0].preserveAttachmentCount = 0;
    subpassDesc[0].pPreserveAttachments = nullptr;



    std::vector<VkSubpassDependency> dependencies;
    dependencies.resize(1);
    dependencies[0] = createSubpassDependency(VK_SUBPASS_EXTERNAL, 0, 
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            0, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, 
            0);

    return createRenderPass(device, attachDesc, subpassDesc, dependencies);
}

VkRenderPass createRenderPass_ColorDepthMSAA(VkPhysicalDevice physicalDevice, VkDevice device, VkFormat format, VkSampleCountFlagBits msaa_samples) 
{
    std::vector<VkAttachmentDescription> attachDesc;
    attachDesc.resize(3);
    attachDesc[0] = createAttachmentDescription(format, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_ATTACHMENT_STORE_OP_STORE, VK_ATTACHMENT_LOAD_OP_CLEAR, msaa_samples);
    attachDesc[1] = createAttachmentDescription(findDepthFormat(physicalDevice), VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, VK_ATTACHMENT_STORE_OP_DONT_CARE, VK_ATTACHMENT_LOAD_OP_CLEAR, msaa_samples);
    attachDesc[2] = createAttachmentDescription(format, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, VK_ATTACHMENT_STORE_OP_STORE, VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_SAMPLE_COUNT_1_BIT);

    VkAttachmentReference colorRefs;
    colorRefs = createAttachmentReference(0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
    VkAttachmentReference depthRef;
    depthRef = createAttachmentReference(1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
    VkAttachmentReference resolveRef;
    resolveRef = createAttachmentReference(2, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

    std::vector<VkSubpassDescription> subpassDesc;
    subpassDesc.resize(1);
    subpassDesc[0].flags = 0;
    subpassDesc[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDesc[0].inputAttachmentCount = 0;
    subpassDesc[0].pInputAttachments = nullptr;
    subpassDesc[0].colorAttachmentCount = 1;
    subpassDesc[0].pColorAttachments = &colorRefs;
    subpassDesc[0].pResolveAttachments = &resolveRef;
    subpassDesc[0].pDepthStencilAttachment = &depthRef;
    subpassDesc[0].preserveAttachmentCount = 0;
    subpassDesc[0].pPreserveAttachments = nullptr;

    std::vector<VkSubpassDependency> dependencies;
    dependencies.resize(1);
    dependencies[0] = createSubpassDependency(VK_SUBPASS_EXTERNAL, 0, 
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            0, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, 
            0);

    return createRenderPass(device, attachDesc, subpassDesc, dependencies);
}


VkResult createFrameBuffer(VkDevice device, VkRenderPass *render_pass, std::vector<VkImageView> image_views, VkExtent2D extent, VkFramebuffer* frame_buffer) {
    VkFramebufferCreateInfo createInfo;
    createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    createInfo.pNext = VK_NULL_HANDLE;
    createInfo.flags = 0;
    createInfo.renderPass = *render_pass;
    createInfo.attachmentCount = image_views.size();
    createInfo.pAttachments = image_views.data();
    createInfo.width = extent.width;
    createInfo.height = extent.height;
    createInfo.layers = 1;

    return vkCreateFramebuffer(device, &createInfo, nullptr, frame_buffer);

    

}

std::vector<VkFramebuffer> createFrameBuffers(VkDevice device, VkRenderPass *render_pass, std::vector<VkImageView> image_views, std::vector<VkImageView> append_images, VkExtent2D extent) {
    std::vector<VkFramebuffer> framebuffers;
    framebuffers.resize(image_views.size());
    for(uint32_t i = 0; i < framebuffers.size(); i++) {
        
        std::vector<VkImageView> image_attachments {image_views[i]};

        // make room for the appended images
        int total_images = image_attachments.size() + append_images.size();
        image_attachments.resize(total_images);

        /* append images. Must be in a specific order. I don't know which order that is yet. */
        for(int j = 0; j < append_images.size(); j++) {
            image_attachments[j] = append_images[j];
        }

        // Append 
        image_attachments[total_images-1] = image_views[i];
        


        if (createFrameBuffer(device, render_pass, image_attachments, extent, &framebuffers[i]) != VK_SUCCESS) {
            framebuffers.resize(0);
            return framebuffers;
        }
    }
    return framebuffers;
}



VkCommandPool createCommandPool(VkDevice device, uint32_t queue_index) {
    VkCommandPoolCreateInfo cmdPoolInfo;
    cmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    cmdPoolInfo.pNext = VK_NULL_HANDLE;
    cmdPoolInfo.flags = 0;
    cmdPoolInfo.queueFamilyIndex = queue_index;

    VkCommandPool graphicsPool;
    if (vkCreateCommandPool(device, &cmdPoolInfo, nullptr, &graphicsPool) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    }

    return graphicsPool;
}

std::vector<VkCommandBuffer> allocateCommandBuffers(VkDevice *device, VkCommandPool *cmdPool, uint32_t amount) {
    VkCommandBufferAllocateInfo cmdAllocInfo;
    cmdAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    cmdAllocInfo.pNext = VK_NULL_HANDLE;
    cmdAllocInfo.commandPool = *cmdPool;
    cmdAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    cmdAllocInfo.commandBufferCount = amount;

    std::vector<VkCommandBuffer> cmdBuffers;
    cmdBuffers.resize(amount);
    if (vkAllocateCommandBuffers(*device, &cmdAllocInfo, cmdBuffers.data()) != VK_SUCCESS){
        cmdBuffers.resize(0);
        return cmdBuffers;
    }

    return cmdBuffers;
}

VkResult cmd_Render(std::vector<VkCommandBuffer> cmdBuffers, 
                    VkRenderPass render_pass, 
                    std::vector<VkFramebuffer> framebuffers, 
                    VkExtent2D extent, 
                    VkPipeline pipeline, 
                    VkPipelineLayout pipeline_layout,
                    std::vector<VkDescriptorSet> descriptorSets,
                    std::vector<Vertex> vertices, 
                    VkBuffer* buffer) 
{
    for(uint32_t i = 0; i < cmdBuffers.size(); i++) {
        
        VkCommandBufferBeginInfo cmdBufferBgnInfo;
        cmdBufferBgnInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        cmdBufferBgnInfo.pNext = VK_NULL_HANDLE;
        cmdBufferBgnInfo.flags = 0;
        cmdBufferBgnInfo.pInheritanceInfo = nullptr;

        if (vkBeginCommandBuffer(cmdBuffers[i], &cmdBufferBgnInfo) != VK_SUCCESS) {
            return VK_NOT_READY;
        }
        VkRenderPassBeginInfo rpBeginInfo;
        rpBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        rpBeginInfo.pNext = VK_NULL_HANDLE;
        rpBeginInfo.renderPass = render_pass;
        rpBeginInfo.framebuffer = framebuffers[i];
        rpBeginInfo.renderArea.offset = {0, 0};
        rpBeginInfo.renderArea.extent = extent;

        std::vector<VkClearValue> clears;
        clears.resize(2);

        clears[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
        clears[1].depthStencil = {1.0f,0};

        rpBeginInfo.clearValueCount = static_cast<uint32_t>(clears.size());
        rpBeginInfo.pClearValues = clears.data();

        vkCmdBeginRenderPass(cmdBuffers[i], &rpBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(cmdBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

        VkBuffer vertexBuffers[] = {*buffer};
        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(cmdBuffers[i], 0, 1, vertexBuffers, offsets);
        
        if (descriptorSets.size() > 0) {
            vkCmdBindDescriptorSets(cmdBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, 
                                    pipeline_layout, 0, 1, &descriptorSets[i], 0, nullptr);
        }

        vkCmdDraw(cmdBuffers[i], static_cast<uint32_t>(vertices.size()), 1, 0, 0);

        vkCmdEndRenderPass(cmdBuffers[i]);

        if (vkEndCommandBuffer(cmdBuffers[i]) != VK_SUCCESS) {
            return VK_NOT_READY;
        }

    }

    return VK_SUCCESS;
}

VkResult cmd_Render_indices(std::vector<VkCommandBuffer> cmdBuffers,
                            VkRenderPass render_pass,
                            std::vector<VkFramebuffer> framebuffers,
                            VkExtent2D extent,
                            VkPipeline pipeline,
                            VkPipelineLayout pipeline_layout,
                            std::vector<VkDescriptorSet> descriptorSets,
                            const std::vector<Vertex> vertices,
                            const std::vector<uint32_t> indices,
                            VkBuffer* vertex_buffer,
                            VkBuffer* index_buffer) 
{
    for(uint32_t i = 0; i < cmdBuffers.size(); i++) {
        
        VkCommandBufferBeginInfo cmdBufferBgnInfo;
        cmdBufferBgnInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        cmdBufferBgnInfo.pNext = VK_NULL_HANDLE;
        cmdBufferBgnInfo.flags = 0;
        cmdBufferBgnInfo.pInheritanceInfo = nullptr;

        if (vkBeginCommandBuffer(cmdBuffers[i], &cmdBufferBgnInfo) != VK_SUCCESS) {
            return VK_NOT_READY;
        }
        VkRenderPassBeginInfo rpBeginInfo;
        rpBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        rpBeginInfo.pNext = VK_NULL_HANDLE;
        rpBeginInfo.renderPass = render_pass;
        rpBeginInfo.framebuffer = framebuffers[i];
        rpBeginInfo.renderArea.offset = {0, 0};
        rpBeginInfo.renderArea.extent = extent;

        std::vector<VkClearValue> clears;
        clears.resize(2);

        clears[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
        clears[1].depthStencil = {1.0f,0};

        rpBeginInfo.clearValueCount = static_cast<uint32_t>(clears.size());
        rpBeginInfo.pClearValues = clears.data();


        vkCmdBeginRenderPass(cmdBuffers[i], &rpBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(cmdBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

            VkBuffer vertexBuffers[] = {*vertex_buffer};
            VkDeviceSize offsets[] = {0};
            vkCmdBindVertexBuffers(cmdBuffers[i], 0, 1, vertexBuffers, offsets);
            vkCmdBindIndexBuffer(cmdBuffers[i], *index_buffer, 0, VK_INDEX_TYPE_UINT32);

            if (descriptorSets.size() > 0) {
                vkCmdBindDescriptorSets(cmdBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
                                        pipeline_layout, 0, 1, &descriptorSets[i], 0, nullptr);
            }

            vkCmdDrawIndexed(cmdBuffers[i], static_cast<uint32_t>(indices.size()), 1, 0, 0, 0);

        vkCmdEndRenderPass(cmdBuffers[i]);

        if (vkEndCommandBuffer(cmdBuffers[i]) != VK_SUCCESS) {
            return VK_NOT_READY;
        }

    }

    return VK_SUCCESS;
}


VkDescriptorSetLayoutBinding create_descriptor_set_layout_binding(uint32_t binding,
                                                                  VkDescriptorType type,
                                                                  uint32_t count,
                                                                  VkShaderStageFlags shader_flags)
{
    VkDescriptorSetLayoutBinding bindInfo;
    bindInfo.binding = binding;
    bindInfo.descriptorType = type;
    bindInfo.descriptorCount = count;
    bindInfo.stageFlags = shader_flags;
    bindInfo.pImmutableSamplers = nullptr;

    return bindInfo;

}

VkDescriptorSetLayout create_descriptor_set_layout(VkDevice device,
                                                           std::vector<VkDescriptorSetLayoutBinding> bindings)
{

    VkDescriptorSetLayoutCreateInfo layoutInfo;
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.pNext = VK_NULL_HANDLE;
    layoutInfo.flags = 0;
    layoutInfo.bindingCount = bindings.size();
    layoutInfo.pBindings = bindings.data();

    VkDescriptorSetLayout descriptorSetLayout;
    
    if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    } else {
        return descriptorSetLayout;
    }

}

VkDescriptorPoolSize create_pool_size(VkDescriptorType type, uint32_t count) {
    VkDescriptorPoolSize poolSize;
    poolSize.type = type;
    poolSize.descriptorCount = count;

    return poolSize;
}

VkDescriptorPool create_descriptor_pool(VkDevice device, std::vector<VkDescriptorPoolSize> poolSizes, uint32_t descriptor_count) {
    
    VkDescriptorPoolCreateInfo poolInfo;
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.pNext = VK_NULL_HANDLE;
    poolInfo.flags = 0;
    poolInfo.maxSets = descriptor_count;
    poolInfo.poolSizeCount = poolSizes.size();
    poolInfo.pPoolSizes = poolSizes.data();

    VkDescriptorPool descriptorPool;
    if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptorPool) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    } else {
        return descriptorPool;
    }
}

std::vector<VkDescriptorSet> create_descriptor_set(VkDevice device, 
                                                   VkDescriptorPool* pool, 
                                                   uint32_t set_count, 
                                                   VkDescriptorSetLayout* layout) 
{
    std::vector<VkDescriptorSetLayout> layouts(set_count, *layout);
    VkDescriptorSetAllocateInfo allocInfo;
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.pNext = VK_NULL_HANDLE;
    allocInfo.descriptorPool = *pool;
    allocInfo.descriptorSetCount = set_count;
    allocInfo.pSetLayouts = layouts.data();

    std::vector<VkDescriptorSet> sets;
    sets.resize(set_count);

    if (vkAllocateDescriptorSets(device, &allocInfo, sets.data()) != VK_SUCCESS) {
        sets.resize(0);
        return sets;
    } else {
        return sets;
    }
}

/** Binding the data with the descriptor sets **/
void bind_descriptor_sets(VkDevice device, 
                          std::vector<VkDescriptorSet> sets, 
                          std::vector<VkDescriptorBufferInfo> desc_buffers, 
                          uint32_t binding_location,
                          VkDeviceSize data_size,
                          VkDescriptorImageInfo *desc_image,
                          uint32_t image_location) 
{
    // amount of descriptor writes to create

    assert(sets.size() == desc_buffers.size());

    size_t max_count = sets.size();

    
    for(size_t i = 0; i < max_count; i++) {
        std::vector<VkWriteDescriptorSet> descriptorWrites;
        descriptorWrites.resize(2);
        
        
        // Uniform
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].pNext = VK_NULL_HANDLE;
        descriptorWrites[0].dstSet = sets[i];
        descriptorWrites[0].dstBinding = binding_location;
        descriptorWrites[0].dstArrayElement = 0;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pBufferInfo = &desc_buffers[i];
        descriptorWrites[0].pImageInfo = nullptr;
        descriptorWrites[0].pTexelBufferView = nullptr;

        // Image
        descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].pNext = VK_NULL_HANDLE;
        descriptorWrites[1].dstSet = sets[i];
        descriptorWrites[1].dstBinding = image_location;
        descriptorWrites[1].dstArrayElement = 0;
        descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pBufferInfo = nullptr;
        descriptorWrites[1].pImageInfo = desc_image;
        descriptorWrites[1].pTexelBufferView = nullptr;

        

        vkUpdateDescriptorSets(device, descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);
    }
}


VkCommandBuffer beginSingleTimeCommands(VkDevice* device, VkCommandPool commandPool) {
    VkCommandBufferAllocateInfo allocInfo;
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.pNext = VK_NULL_HANDLE;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = commandPool;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(*device, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo;
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.pNext = VK_NULL_HANDLE;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    beginInfo.pInheritanceInfo = nullptr;

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    return commandBuffer;

}

void endSingleTimeCommands(VkDevice device, VkCommandPool pool, VkCommandBuffer commandBuffer, VkQueue* queue) {
    vkEndCommandBuffer(commandBuffer);

    // execute command
    VkSubmitInfo submitInfo;
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.pNext = VK_NULL_HANDLE;
    submitInfo.waitSemaphoreCount = 0;
    submitInfo.pWaitSemaphores = nullptr;
    submitInfo.pWaitDstStageMask = nullptr;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;
    submitInfo.signalSemaphoreCount = 0;
    submitInfo.pSignalSemaphores = nullptr;

    vkQueueSubmit(*queue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(*queue);     // wait for completion

    vkFreeCommandBuffers(device, pool, 1, &commandBuffer);

}

// Transition image layout to have the command contain the right layout
void transitionImageLayout(VkDevice device, 
                           VkCommandPool pool,
                           VkQueue* queue,
                           VkImage image, 
                           VkFormat format, 
                           VkImageLayout oldLayout, 
                           VkImageLayout newLayout,
                           uint32_t mips_level
                           ) {
    VkCommandBuffer commandBuffer = beginSingleTimeCommands(&device, pool);

    VkImageMemoryBarrier barrier;
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.pNext = VK_NULL_HANDLE;
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    // used to transfer queue ownership
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

    barrier.image = image;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = mips_level;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;

    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;

    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else {
        return;
    }
    

    vkCmdPipelineBarrier(
        commandBuffer,
        sourceStage,destinationStage,
        0,
        0, nullptr,
        0, nullptr,
        1, &barrier
    );


    endSingleTimeCommands(device, pool, commandBuffer, queue);
}

void copyBufferToImage(VkDevice device,
                       VkCommandPool pool,
                       VkQueue* queue,
                       VkBuffer* buffer, VkImage* image, uint32_t width, uint32_t height) {
    VkCommandBuffer commandBuffer = beginSingleTimeCommands(&device, pool);

    // specify which parts of the buffer are going to be coppied to the image
    VkBufferImageCopy region;
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0 ;
    region.imageSubresource.baseArrayLayer = 0; 
    region.imageSubresource.layerCount = 1;

    region.imageOffset = {0, 0, 0};
    region.imageExtent = {
        width,
        height,
        1
    };

    vkCmdCopyBufferToImage(
        commandBuffer,
        *buffer,
        *image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &region
    );

    endSingleTimeCommands(device, pool, commandBuffer, queue);
}

VkSampler createSampler(VkPhysicalDevice physicalDevice, VkDevice device, VkFilter filter, VkSamplerAddressMode address_mode, VkSamplerMipmapMode mode, uint32_t mip_levels) {
   VkSamplerCreateInfo samplerInfo;
   samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
   samplerInfo.pNext = VK_NULL_HANDLE;
   samplerInfo.flags = 0;
   samplerInfo.magFilter = filter;
   samplerInfo.minFilter = filter;
   samplerInfo.mipmapMode = mode;
   samplerInfo.addressModeU = address_mode;
   samplerInfo.addressModeV = address_mode;
   samplerInfo.addressModeW = address_mode;
   samplerInfo.mipLodBias = 0.0f;
   samplerInfo.anisotropyEnable = VK_TRUE;

    VkPhysicalDeviceProperties properties;
    vkGetPhysicalDeviceProperties(physicalDevice, &properties);

   samplerInfo.maxAnisotropy = properties.limits.maxSamplerAnisotropy;
   samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
   samplerInfo.unnormalizedCoordinates = VK_FALSE;
   samplerInfo.compareEnable = VK_FALSE;
   samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
   samplerInfo.minLod = 0.0f;
   samplerInfo.maxLod = (float) mip_levels;

   VkSampler sampler;

   if (vkCreateSampler(device, &samplerInfo, nullptr, &sampler) != VK_SUCCESS) {
       return VK_NULL_HANDLE;
   } else {
       return sampler;
   }
}

VkFormat findSupportedFormat(VkPhysicalDevice physicalDevice, const std::vector<VkFormat>& candidates,VkImageTiling tiling, VkFormatFeatureFlags features) {
    for (VkFormat format : candidates) {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);
        if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
            return format;
        } else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) 
        {
            return format;
        }
    }

    return VK_FORMAT_UNDEFINED;

}

VkFormat findDepthFormat(VkPhysicalDevice physicalDevice) {
    return findSupportedFormat(physicalDevice,
                        {VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT},
                        VK_IMAGE_TILING_OPTIMAL,
                        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

bool hasStencilComponent(VkFormat format) {
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

VkResult generateMipMaps(VkPhysicalDevice physicalDevice, VkDevice device, VkCommandPool pool, VkQueue queue, VkImage image, VkFormat format, int32_t tex_width, int32_t tex_height, uint32_t mip_levels) {

    // Check if image format supports linear blitting
    VkFormatProperties format_properties;
    vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &format_properties);

    if (! (format_properties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT)) {
        return VK_NOT_READY;
    }

    VkCommandBuffer cmd_buffer = beginSingleTimeCommands(&device, pool);

    VkImageMemoryBarrier barrier;
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.pNext = VK_NULL_HANDLE;
    barrier.image = image;
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = 0;
    barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    barrier.newLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;
    barrier.subresourceRange.levelCount = 1;

    int32_t mip_width = tex_width;
    int32_t mip_height = tex_height;

    for (uint32_t i = 1; i < mip_levels; i++) {
        barrier.subresourceRange.baseMipLevel = i - 1;
        barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

        vkCmdPipelineBarrier(cmd_buffer,
                VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
                0, nullptr,
                0, nullptr,
                1, &barrier);

        // specify regions for blit operations
        VkImageBlit blit;
        blit.srcOffsets[0] = {0, 0, 0};
        blit.srcOffsets[1] = { mip_width, mip_height, 1};
        blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        blit.srcSubresource.mipLevel = i - 1;
        blit.srcSubresource.baseArrayLayer = 0;
        blit.srcSubresource.layerCount = 1;
        blit.dstOffsets[0] = {0,0,0};
        blit.dstOffsets[1] = { mip_width > 1 ? mip_width / 2 : 1, mip_height > 1 ? mip_height / 2: 1, 1};
        blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        blit.dstSubresource.mipLevel = i;
        blit.dstSubresource.baseArrayLayer = 0;
        blit.dstSubresource.layerCount = 1;

        vkCmdBlitImage(cmd_buffer,
                image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                1, &blit,
                VK_FILTER_LINEAR);

        barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        vkCmdPipelineBarrier(cmd_buffer,
                VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
                0, nullptr,
                0, nullptr,
                1, &barrier);

        // ensure division does not become 0
        if (mip_width > 1) mip_width /= 2;
        if (mip_height > 1) mip_height /= 2;

        
    }

    // transition last mip level
    barrier.subresourceRange.baseMipLevel = mip_levels - 1;
    barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    vkCmdPipelineBarrier(cmd_buffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
            0, nullptr,
            0, nullptr,
            1, &barrier);

    endSingleTimeCommands(device, pool, cmd_buffer, &queue);
    return VK_SUCCESS;

}

VkSampleCountFlagBits getMaxUseableCount(VkPhysicalDevice physicalDevice) {
    VkPhysicalDeviceProperties properties;
    vkGetPhysicalDeviceProperties(physicalDevice, &properties);

    VkSampleCountFlags counts = properties.limits.framebufferColorSampleCounts & properties.limits.framebufferDepthSampleCounts;

    if (counts & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
    if (counts & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
    if (counts & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
    if (counts & VK_SAMPLE_COUNT_8_BIT)  { return VK_SAMPLE_COUNT_8_BIT; }
    if (counts & VK_SAMPLE_COUNT_4_BIT)  { return VK_SAMPLE_COUNT_4_BIT; }
    if (counts & VK_SAMPLE_COUNT_2_BIT)  { return VK_SAMPLE_COUNT_2_BIT; }

    return VK_SAMPLE_COUNT_1_BIT;
}

