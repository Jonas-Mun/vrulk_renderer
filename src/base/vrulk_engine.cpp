#include "vrulk_engine.h"
#include "vertex.h"
#include <set>

/** Swapchain **/

VrulkSwapchain::VrulkSwapchain(VkPhysicalDevice phyDevice, VkDevice device,
                               std::set<uint32_t> queue_set, VkSurfaceKHR surface,
                               GLFWwindow** window)
{
    surface_format = surfaceFormat(phyDevice, surface);
    extent = surfaceExtent(phyDevice, surface, window);
    present_mode = surfacePresentModes(phyDevice, surface);
    min_images = surfaceMinImageCount(phyDevice, surface);
    swapchain = createSwapchain(phyDevice, device, surface, surface_format, present_mode,
                                extent, min_images, queue_set, window, nullptr);

    if (swapchain == VK_NULL_HANDLE) {
        throw std::runtime_error("Failed to create swapchain");
    }

    uint32_t swapchain_image_count = 0;
    vkGetSwapchainImagesKHR(device, swapchain, &swapchain_image_count, nullptr);
    images.resize(swapchain_image_count);
    VkResult result = vkGetSwapchainImagesKHR(device, swapchain, &swapchain_image_count, images.data());
    if (result != VK_SUCCESS) {
        throw std::runtime_error("Failed to get images");
    }
    image_views = createImageViews(device, swapchain_image_count, images, surface_format.format, VK_IMAGE_ASPECT_COLOR_BIT, 1);
}

VrulkSwapchain::~VrulkSwapchain() {
}

VkExtent2D VrulkSwapchain::get_extent() {
    return extent;
}

VkPresentModeKHR VrulkSwapchain::get_present_mode() {
    return present_mode;
}

VkSurfaceFormatKHR VrulkSwapchain::get_surface_format() {
    return surface_format;
}

VkSwapchainKHR VrulkSwapchain::get_swapchain() {
    return swapchain;
}

uint32_t VrulkSwapchain::minimum_images() {
    return min_images;
}

void VrulkSwapchain::clean_up(VkPhysicalDevice physicalDevice, VkDevice device) {
    for (uint32_t i = 0; i < image_views.size(); i++) {
        vkDestroyImageView(device, image_views[i], nullptr);
    }

    vkDestroySwapchainKHR(device, swapchain, nullptr);
}

/***********/


/** Pipeline **/
VrulkPipeline::VrulkPipeline(VkPhysicalDevice physicalDevice, VkDevice device, 
        VrulkSwapchain swapchain, VkDescriptorSetLayout* desc_layout, 
        VkPipelineShaderStageCreateInfo shader_stages[], 
        uint32_t shader_amount, uint32_t queue_family, 
        VkSampleCountFlagBits msaa) 
    : depth_buffer(physicalDevice, device, swapchain.get_extent().width, swapchain.get_extent().height, msaa), color_image()
{


    msaa_sample = msaa;
    render_pass = createRenderPass_ColorDepthMSAA(physicalDevice, device, swapchain.get_surface_format().format, msaa);
    if (render_pass == VK_NULL_HANDLE) {
        throw std::runtime_error("Failed to create render_pass");
    }

    std::vector<VkDescriptorPoolSize> poolSizes;
    poolSizes.resize(2);
    poolSizes[0] = create_pool_size(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, swapchain.images.size());
    poolSizes[1] = create_pool_size(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, swapchain.images.size());
    descriptor_pool = create_descriptor_pool(device, poolSizes, swapchain.images.size());
    descriptor_sets = create_descriptor_set(device, &descriptor_pool, swapchain.images.size(), desc_layout);

    VkPipelineLayoutCreateInfo layoutInfo;
    layoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    layoutInfo.pNext = VK_NULL_HANDLE;
    layoutInfo.flags = 0;
    layoutInfo.setLayoutCount = 1;
    layoutInfo.pSetLayouts = desc_layout;
    layoutInfo.pushConstantRangeCount = 0;
    layoutInfo.pPushConstantRanges = nullptr;

    if (vkCreatePipelineLayout(device, &layoutInfo, nullptr, &pipeline_layout) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create pipeline layout");
    }
    std::vector<VkVertexInputAttributeDescription> attr_desc = vertex_attribute_description();
    VkVertexInputBindingDescription bind_desc = vertex_binding_description();
    graphics_pipeline = createGraphicsPipeline(device, render_pass, shader_stages, pipeline_layout, swapchain.get_extent(), &bind_desc, msaa,true, attr_desc);

    if (graphics_pipeline == VK_NULL_HANDLE) {
        throw std::runtime_error("Failed to create graphics pipeline");
    }

    color_image.create_image(physicalDevice, device, 
            swapchain.get_extent().width, swapchain.get_extent().height, 1, 
            swapchain.get_surface_format().format, VK_IMAGE_TILING_OPTIMAL, 
            msaa_sample, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, 
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_COLOR_BIT);


    // Must be in a specific order. I don't know which order that is yet
    std::vector<VkImageView> append_images = { color_image.get_view(), depth_buffer.get_view()};

    frame_buffers = createFrameBuffers(device, &render_pass, swapchain.image_views, append_images, swapchain.get_extent());

    }

VrulkPipeline::~VrulkPipeline() {
}

VkRenderPass VrulkPipeline::get_render_pass() {
    return render_pass;
}

VkPipeline VrulkPipeline::get_pipeline() {
    return graphics_pipeline;
}

VkPipelineLayout VrulkPipeline::get_pipeline_layout() {
    return pipeline_layout;
}

VkDescriptorPool VrulkPipeline::get_descriptor_pool() {
    return descriptor_pool;
}

void VrulkPipeline::clean_up(VkDevice device) {
    color_image.clean_up(device);
    depth_buffer.clean_up(device);
    for (uint32_t i = 0; i < frame_buffers.size(); i++) {
        vkDestroyFramebuffer(device, frame_buffers[i], nullptr);
    }

    vkDestroyPipeline(device, graphics_pipeline, nullptr);
    vkDestroyPipelineLayout(device, pipeline_layout, nullptr);
    vkDestroyRenderPass(device, render_pass, nullptr);
    vkDestroyDescriptorPool(device, descriptor_pool, nullptr);
}

/*******************/

/** Buffers **/

VrulkBuffer::VrulkBuffer(VkPhysicalDevice physicalDevice, VkDevice device, VkBufferUsageFlags buffer_usage, VkDeviceSize object_size, uint32_t amount) {
    buffers.resize(amount);
    memory.resize(amount);

    for (size_t i = 0; i < amount; i++) {
        buffers[i] = createBuffer(device, object_size, buffer_usage);
        allocateBufferMemory(physicalDevice, device, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &buffers[i], &memory[i]);
        bindBuffer(device, object_size, &buffers[i], &memory[i]);
    }
}

VrulkBuffer::VrulkBuffer(VkPhysicalDevice physicalDevice, VkDevice device, 
                        VkCommandPool command_pool, VkQueue queue, VkBufferUsageFlags usage_flags,
                        VkDeviceSize buffer_size, const void * buffer_data, uint32_t buffer_count)
{
    buffers.resize(buffer_count);
    memory.resize(buffer_count);
    for (size_t i = 0; i < buffer_count; i++) {
        create_mapped_buffer(physicalDevice, device, command_pool, &queue, 
                             buffer_size, usage_flags,
                             &buffers[i],
                             (&(memory[i])), buffer_data);
    }
}

VrulkBuffer::~VrulkBuffer() {
}

void VrulkBuffer::clean_up(VkDevice device) {
    printf("Freeing things\n");
    for (size_t i = 0; i < buffers.size(); i++) {
        vkDestroyBuffer(device, buffers[i], nullptr);
    }
    for(size_t i = 0; i < memory.size(); i++) {
        printf("Freeing uniform buffer\n");
        vkFreeMemory(device, memory[i], nullptr);
    }
}

/**************/

/** Command Buffers **/

VrulkCommands::VrulkCommands(VkDevice device, uint32_t queue_family, uint32_t command_count) {
    pool = createCommandPool(device, queue_family);
    if (pool == VK_NULL_HANDLE) {
        throw std::runtime_error("Failed to create command pool");
    }

    command_buffers = allocateCommandBuffers(&device, &pool, command_count);
}

VrulkCommands::~VrulkCommands() {
}

void VrulkCommands::clean_up(VkDevice device) {
    vkDestroyCommandPool(device, pool, nullptr);
}

/** Texture **/
VrulkTexture::VrulkTexture(VkPhysicalDevice physicalDevice, VkDevice device, 
                           unsigned char** data, int width, int height, int channels, uint32_t mips_level,
                           VkCommandPool command_pool, VkQueue *queue) {
    VkDeviceSize texture_size;
    texture_size = width * height * 4;
    


    VkBuffer staging_texture_buffer = createBuffer(device, texture_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
    VkDeviceMemory *texture_memory = (VkDeviceMemory *) malloc(sizeof(VkDeviceMemory));
    allocateBufferMemory(physicalDevice, device, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, &staging_texture_buffer, texture_memory);
    bindBuffer(device, texture_size, &staging_texture_buffer, texture_memory);
    mapData(device, texture_memory, texture_size, (const void *)*data);


    image = createImage(device, width, height, mips_level,
            VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, VK_SAMPLE_COUNT_1_BIT, 
            VK_IMAGE_USAGE_TRANSFER_DST_BIT|VK_IMAGE_USAGE_TRANSFER_SRC_BIT|VK_IMAGE_USAGE_SAMPLED_BIT, 
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) ;

    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(device, image, &memRequirements);

    VkMemoryAllocateInfo allocInfo;
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.pNext = VK_NULL_HANDLE;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = findMemoryType(physicalDevice, memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    if (vkAllocateMemory(device, &allocInfo, nullptr, &memory) != VK_SUCCESS) {
        throw std::runtime_error("Failed to allocate texture memory");
    }

    vkBindImageMemory(device, image, memory, 0);

    // Transfer buffer data to image object
    //1) transition texture image to VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    transitionImageLayout(device, command_pool, queue, image, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, mips_level);
    copyBufferToImage(device, command_pool, queue, &staging_texture_buffer, &image, width, height);

    /* Mips level */
    // Having the image be blipped will make it be READ ONLY SHADER
    // allow sampling in shader
    //transitionImageLayout(device, command_pool, queue, image, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, mips_level);

    generateMipMaps(physicalDevice, device, command_pool, *queue, image, VK_FORMAT_R8G8B8A8_SRGB, width, height, mips_level);


    //2) Execute buffer to image copy operation
    vkDestroyBuffer(device, staging_texture_buffer, nullptr);
    vkFreeMemory(device, *texture_memory, nullptr);
    free(texture_memory);

    image_view = createImageView(device, image, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_ASPECT_COLOR_BIT, mips_level);
    sampler = createSampler(physicalDevice, device, VK_FILTER_LINEAR, VK_SAMPLER_ADDRESS_MODE_REPEAT, VK_SAMPLER_MIPMAP_MODE_LINEAR, mips_level);

}

VkImageView VrulkTexture::get_view() {
    return image_view;
}
VkImage VrulkTexture::get_image() {
    return image;
}
VkSampler VrulkTexture::get_sampler() {
    return sampler;
}

void VrulkTexture::clean_up(VkDevice device) {
    vkDestroySampler(device, sampler, nullptr);
    vkDestroyImageView(device, image_view, nullptr); 
    vkDestroyImage(device, image, nullptr);
    vkFreeMemory(device, memory, nullptr);

}

/** Depth Buffer **/
VrulkDepthBuffer::VrulkDepthBuffer(VkPhysicalDevice physicalDevice, VkDevice device, int width, int height, VkSampleCountFlagBits sample_count) {
    format = findDepthFormat(physicalDevice);
    image = createImage(device, width, height, 1, 
            format,VK_IMAGE_TILING_OPTIMAL, sample_count,
            VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    allocateImageMemory(physicalDevice, device, image, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &memory);

    bindImageMemory(device, &image, &memory); // bind before creating image view
    view = createImageView(device, image, format, VK_IMAGE_ASPECT_DEPTH_BIT, 1);
}

VkImage VrulkDepthBuffer::get_image() {
    return image;
}

VkImageView VrulkDepthBuffer::get_view() {
    return view;
}

VkFormat VrulkDepthBuffer::get_format() {
    return format;
}

VkDeviceMemory VrulkDepthBuffer::get_memory() {
    return memory;
}

void VrulkDepthBuffer::clean_up(VkDevice device) {
    vkDestroyImageView(device, view, nullptr);
    vkDestroyImage(device, image, nullptr);
    vkFreeMemory(device, memory, nullptr);
}

VrulkDepthBuffer::~VrulkDepthBuffer() {
}


VrulkImage::VrulkImage() {
    memory = nullptr;
    image = nullptr;
    image_view = nullptr;
}
VrulkImage::VrulkImage(VkPhysicalDevice physicalDevice, VkDevice device, 
                       unsigned char** data, int width, int height, uint32_t mips_level, 
                       VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, 
                       VkSampleCountFlagBits sample_count,
                       VkMemoryPropertyFlags properties, VkImageAspectFlags aspect_flags)
{
    image = createImage(device, width, height, mips_level, 
            format, tiling, sample_count,
            usage, properties);
    allocateImageMemory(physicalDevice, device, image, properties, &memory);
    bindImageMemory(device, &image, &memory);
    image_view = createImageView(device, image, format, aspect_flags, mips_level);

}

VkImageView VrulkImage::get_view() {
    return image_view;
}

VkImage VrulkImage::get_image() {
    return image;
}

VkDeviceMemory VrulkImage::get_memory() {
    return memory;
}

void VrulkImage::clean_up(VkDevice device) {
    vkDestroyImageView(device, image_view, nullptr);
    vkDestroyImage(device, image, nullptr);
    vkFreeMemory(device, memory, nullptr);
}

void VrulkImage::create_image(VkPhysicalDevice physicalDevice, VkDevice device, int width, int height, uint32_t mips_level,
                      VkFormat format, VkImageTiling tiling, VkSampleCountFlagBits sample_count,
                      VkImageUsageFlags usage, VkMemoryPropertyFlags properties,
                      VkImageAspectFlags aspect_flags) 
{
    image = createImage(device, width, height, mips_level, format, tiling, sample_count, usage, properties);
    allocateImageMemory(physicalDevice, device, image, properties, &memory);
    bindImageMemory(device, &image, &memory);
    image_view = createImageView(device, image, format, aspect_flags, mips_level);
}



VrulkEngineTools* create_vrulk_tools(std::set<uint32_t> queue_set, 
                                     std::vector<VkPipelineShaderStageCreateInfo> shader_stages, 
                                     VkSurfaceKHR surface, 
                                     uint32_t shader_amount, 
                                     uint32_t queue_index, 
                                     GLFWwindow** window,
                                     std::vector<Vertex> vertices,
                                     std::vector<uint16_t> indices
                                     ) 
{
    VrulkEngineTools* tools = new(VrulkEngineTools);
    tools->queue_set = queue_set;
    tools->shader_stages = shader_stages;
    tools->surface = surface;
    tools->shader_amount = shader_amount;
    tools->queue_index = queue_index;
    tools->window = window;
    tools->vertices = vertices;
    tools->indices = indices;

    return tools;
}

