#ifndef BASE_VRULK_H
#define BASE_VRULK_H
#include "vulkan_header.h"
#include "queue_family.h"
#include "vertex.h"
#include <stdio.h> 
#include <vector>
//#include <vulkan/vulkan.h>
//
//

#include "vrulk_utils.h"
#include "base_image.h"
#include "base_buffer.h"

/**
 * Helper structure
 */

/**
 * Helper functions
 */
bool checkValidationSupport(const char** layers, int layer_size);
void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT* debugInfo);
bool graphicsCompatible(VkPhysicalDevice physicalDevice, VkPhysicalDeviceProperties properties);
struct QueueFamily findQueue(VkPhysicalDevice physicalDevice, VkQueueFlagBits flagBit);
VkResult CreateDebugUtilsMessengerEXT(VkInstance instance,
                                      const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
                                      const VkAllocationCallbacks* pAllocator,
                                      VkDebugUtilsMessengerEXT* pDebugMessenger
        );
VkDebugUtilsMessengerEXT setupDebugMessenger(VkInstance instance, FILE** fInfo);

std::vector<VkDescriptorBufferInfo> create_buffer_descriptors(std::vector<VkBuffer> buffers, VkDeviceSize data_size);


   


/**
 * Main Functions
 */

/*
 * @app_name - name of the app
 * @engine_name - name of the engine
 * @engine_versions - version of the engine
 * @instance_extensions - extensions of the instances such as SwapchainKHR
 * @validation_layers - provides validation layers if true
 * @fInfo - FILE stream for information
 * ---
 *  returns: an instance with given extensions
 */
VkInstance createInstance(const char* app_name, uint32_t app_version, 
                          const char*engine_name,uint32_t engine_version,
                          std::vector<const char *> instance_extensions,
                          bool validation_layers, FILE** fInfo);

/*
 * @instance - valid Vulkan instance
 * @fInfo - FILE stream for information
 * ---
 *  returns: an external Physical Device with the Graphics Bit 
 */
VkPhysicalDevice createPhysicalDevice(VkInstance instance, FILE** fInfo);

/*
 * @physicalDevice - a valid physical device
 * @extension_queues - queues that the logical device must create
 * @device_extensions - extensions of the device
 * @fInfo - FILE stream for information
 * ---
 *  returns: a logical device the supports the given queues and device extensions
 */
VkDevice createLogicalDevice(VkPhysicalDevice physicalDevice, 
                             std::vector<struct QueueFamily> extension_queues, 
                             std::vector<const char *> device_extensions,
                             FILE** fInfo);

/*
 *
 *
 */

/** Pipeline Functions **/
VkShaderModule createShaderModule(VkDevice device, std::vector<char> code);

VkPipelineShaderStageCreateInfo createShaderStage(VkDevice device, VkShaderModule module, const char * entry, VkShaderStageFlagBits flag_bit);

VkPipeline createGraphicsPipeline(VkDevice device, VkRenderPass render_pass, 
                                  VkPipelineShaderStageCreateInfo* shaders, 
                                  VkPipelineLayout pipeline_layout,
                                  VkExtent2D extent,
                                  VkVertexInputBindingDescription* binding_description,
                                  VkSampleCountFlagBits msaa_samples,
                                  bool depth,
                                  std::vector<VkVertexInputAttributeDescription> attribute_description
                                  );


/** Render Pass **/
//VkSubpassDescription createSubpassDescription(std::vector<VkAttachmentReference> color, VkAttachmentReference* depth);

VkAttachmentDescription createAttachmentDescription(VkFormat format, VkImageLayout final_layout, VkAttachmentStoreOp store_op, VkAttachmentLoadOp load_op, VkSampleCountFlagBits msaa_samples);

VkAttachmentReference createAttachmentReference(uint32_t index, VkImageLayout layout);

VkSubpassDependency createSubpassDependency(uint32_t src_sub, uint32_t dst_sub, VkPipelineStageFlags src_stage_mask, VkPipelineStageFlags dst_stage_mask, VkAccessFlags src_access_mask, VkAccessFlags dst_access_mask, VkDependencyFlags dependencies) ;

VkRenderPass createRenderPass(VkDevice device, std::vector<VkAttachmentDescription> attach_desc, std::vector<VkSubpassDescription> subpass_desc, std::vector<VkSubpassDependency> subpass_dependency);

VkRenderPass createRenderPass_Color(VkDevice, VkFormat format);
VkRenderPass createRenderPass_ColorDepth(VkPhysicalDevice physicalDevice, VkDevice device, VkFormat format);
VkRenderPass createRenderPass_ColorDepthMSAA(VkPhysicalDevice physicalDevice, VkDevice device, VkFormat format, VkSampleCountFlagBits msaa_samples);

/** Frame Buffer **/
std::vector<VkFramebuffer> createFrameBuffers(VkDevice device, VkRenderPass *render_pass, std::vector<VkImageView> image_views, std::vector<VkImageView> append_images, VkExtent2D extent);


/** Command Buffer **/
VkCommandPool createCommandPool(VkDevice device, uint32_t queue_index);
std::vector<VkCommandBuffer> allocateCommandBuffers(VkDevice *device, 
                                                    VkCommandPool *cmdPool, 
                                                    uint32_t amount);

/** Commands **/

VkResult cmd_Render(std::vector<VkCommandBuffer> cmdBuffers, 
                    VkRenderPass render_pass, 
                    std::vector<VkFramebuffer> framebuffers, 
                    VkExtent2D extent, 
                    VkPipeline pipeline, 
                    VkPipelineLayout pipeline_layout,
                    std::vector<VkDescriptorSet> descriptorSets,
                    std::vector<Vertex> vertices, 
                    VkBuffer* buffer);
VkResult cmd_Render_indices(std::vector<VkCommandBuffer> cmdBuffers,
                            VkRenderPass render_pass,
                            std::vector<VkFramebuffer> framebuffers,
                            VkExtent2D extent,
                            VkPipeline pipeline,
                            VkPipelineLayout pipeline_layout,
                            std::vector<VkDescriptorSet> descriptorSets,
                            const std::vector<Vertex> vertices,
                            const std::vector<uint32_t> indices,
                            VkBuffer* vertex_buffer,
                            VkBuffer* index_buffer);


VkDescriptorSetLayoutBinding create_descriptor_set_layout_binding(uint32_t binding,
                                                                  VkDescriptorType type,
                                                                  uint32_t count,
                                                                  VkShaderStageFlags shader_flags);

VkDescriptorSetLayout create_descriptor_set_layout(VkDevice device, 
                                                   std::vector<VkDescriptorSetLayoutBinding> bindings);
VkDescriptorPoolSize create_pool_size(VkDescriptorType type, uint32_t count);

VkDescriptorPool create_descriptor_pool(VkDevice device, std::vector<VkDescriptorPoolSize> poolSizes, uint32_t descriptor_count);

std::vector<VkDescriptorSet> create_descriptor_set(VkDevice device, 
                                                   VkDescriptorPool* pool, 
                                                   uint32_t set_count, 
                                                   VkDescriptorSetLayout* layout);

void bind_descriptor_sets(VkDevice device, 
                          std::vector<VkDescriptorSet> sets, 
                          std::vector<VkDescriptorBufferInfo> desc_buffers, 
                          uint32_t binding_location,
                          VkDeviceSize data_size,
                          VkDescriptorImageInfo *desc_image,
                          uint32_t image_location);



VkCommandBuffer beginSingleTimeCommands(VkDevice* device, VkCommandPool commandPool);
void endSingleTimeCommands(VkDevice device, VkCommandPool pool, VkCommandBuffer commandBuffer, VkQueue* queue);


void transitionImageLayout(VkDevice device, 
                           VkCommandPool pool,
                           VkQueue* queue,
                           VkImage image, 
                           VkFormat format, 
                           VkImageLayout oldLayout, 
                           VkImageLayout newLayout,
                           uint32_t mips_level
                           );
 void copyBufferToImage(VkDevice device,
                       VkCommandPool pool,
                       VkQueue* queue,
                       VkBuffer* buffer, VkImage* image, uint32_t width, uint32_t height);

VkSampler createSampler(VkPhysicalDevice physicalDevice, VkDevice device, VkFilter filter, VkSamplerAddressMode address_mode, VkSamplerMipmapMode mode, uint32_t mip_levels);

VkFormat findSupportedFormat(VkPhysicalDevice physicalDevice, const std::vector<VkFormat>& candidates,VkImageTiling tiling, VkFormatFeatureFlags features);
VkFormat findDepthFormat(VkPhysicalDevice physicalDevice);
bool hasStencilComponent(VkFormat format);

VkResult generateMipMaps(VkPhysicalDevice physicalDevice, VkDevice device, VkCommandPool pool, VkQueue queue, VkImage image, VkFormat format, int32_t tex_width, int32_t tex_height, uint32_t mip_levels);

VkSampleCountFlagBits getMaxUseableCount(VkPhysicalDevice physicalDevice);

#endif


