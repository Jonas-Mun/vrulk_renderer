/**
 * Common header for files that need to access
 * the vulkan api.
 *
 * Can be better, but for now it should do
 * the job.
 */

#ifndef VULKAN_HEADER_H
#define VULKAN_HEADER_H

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>



#endif
