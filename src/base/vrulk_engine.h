#ifndef VRULK_ENGINE_H
#define VRULK_ENGINE_H
#include "base_vrulk.h"
#include "ext/vulk_surface.h"
#include "ext/vulk_swapchain.h"
#include "vertex.h"
/*
 * A CPP wrapper over the Vulkan API
 */

/** Vrulk Engine Data structures **/

class VrulkSwapchain {
private:

    VkSwapchainKHR swapchain;
    VkSurfaceFormatKHR surface_format;
    VkPresentModeKHR present_mode;
    VkExtent2D extent;
    uint32_t min_images;
public:
    std::vector<VkImage> images;
    std::vector<VkImageView> image_views;

    VrulkSwapchain(VkPhysicalDevice phyDevice, VkDevice device,
                   std::set<uint32_t> queue_set, VkSurfaceKHR surface,
                   GLFWwindow** window);
    ~VrulkSwapchain();

    VkExtent2D get_extent();
    VkPresentModeKHR get_present_mode();
    VkSurfaceFormatKHR get_surface_format();
    VkSwapchainKHR get_swapchain();
    uint32_t minimum_images();

    void clean_up(VkPhysicalDevice physicalDevice, VkDevice device);

};

class VrulkDepthBuffer {
private:
    VkImage image;
    VkImageView view;
    VkDeviceMemory memory;
    VkFormat format;
public:
    VrulkDepthBuffer(VkPhysicalDevice physicalDevice, VkDevice device, int width, int height, VkSampleCountFlagBits sample_count);
    ~VrulkDepthBuffer();

    VkImage get_image();
    VkImageView get_view();
    VkFormat get_format();
    VkDeviceMemory get_memory();

    void clean_up(VkDevice device);

};

class VrulkImage {
private:
    VkDeviceMemory memory;
    VkImage image;
public:
    VkImageView image_view;
    VrulkImage();
    VrulkImage(VkPhysicalDevice physicalDevice, VkDevice device, 
               unsigned char** data, int widht, int height, uint32_t mips_level, 
               VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, 
               VkSampleCountFlagBits sample_count,
               VkMemoryPropertyFlags properties, VkImageAspectFlags aspect_flags);

    void clean_up(VkDevice device);

    VkImageView get_view();
    VkImage get_image();
    VkDeviceMemory get_memory();
    
    void create_image(VkPhysicalDevice physicalDevice, VkDevice device, int width, int height, uint32_t mips_level,
                      VkFormat format, VkImageTiling tiling, VkSampleCountFlagBits sample_count,
                      VkImageUsageFlags usage, VkMemoryPropertyFlags properties,
                      VkImageAspectFlags aspect_flags);

};

class VrulkPipeline {
private:

    VkSampleCountFlagBits msaa_sample;
    VkRenderPass render_pass;
    VkPipeline graphics_pipeline;
    VkPipelineLayout pipeline_layout;
    VkDescriptorPool descriptor_pool;
    VrulkDepthBuffer depth_buffer;
    VrulkImage color_image;
public:
    std::vector<VkFramebuffer> frame_buffers;
    std::vector<VkDescriptorSet> descriptor_sets;

    VrulkPipeline(VkPhysicalDevice physicalDevice, VkDevice device, VrulkSwapchain swapchain, VkDescriptorSetLayout* desc_layout,
                  VkPipelineShaderStageCreateInfo shader_stages[],
                  uint32_t shader_amount, uint32_t queue_family, VkSampleCountFlagBits msaa);
    ~VrulkPipeline();

    VkRenderPass get_render_pass();
    VkPipeline get_pipeline();
    VkPipelineLayout get_pipeline_layout();
    VkDescriptorPool get_descriptor_pool();
    VrulkDepthBuffer get_depth_buffer();


    void clean_up(VkDevice device);
};

class VrulkBuffer {
public:
    std::vector<VkBuffer> buffers;
    std::vector<VkDeviceMemory> memory;

    VrulkBuffer(VkPhysicalDevice physicalDevice, VkDevice device, VkBufferUsageFlags buffer_usage, VkDeviceSize object_size, uint32_t amount);
    VrulkBuffer(VkPhysicalDevice physicalDevice, VkDevice device, 
                VkCommandPool command_pool, VkQueue queue, VkBufferUsageFlags usage_flags,
                VkDeviceSize buffer_size, const void * buffer_data, uint32_t buffer_count);
    ~VrulkBuffer();

    void clean_up(VkDevice device);
};


class VrulkCommands {
public:
    VkCommandPool pool;
    std::vector<VkCommandBuffer> command_buffers;

    VrulkCommands(VkDevice device, uint32_t queue_family, uint32_t command_count);
    ~VrulkCommands();

    void clean_up(VkDevice device);
};

class VrulkTexture {
private:
    VkDeviceMemory memory;
    VkImage image;
public:
    VkImageView image_view;
    VkSampler sampler;

    VkImageView get_view();
    VkImage get_image();
    VkSampler get_sampler();

    void clean_up(VkDevice device);

    VrulkTexture(VkPhysicalDevice physicalDevice, VkDevice device, 
                 unsigned char** data, int width, int height, int channels, uint32_t mips_level,
                 VkCommandPool command_pool, VkQueue* queue);

};



/* Stores objects
 * needed to create an engine
 */
typedef struct VrulkEngineTools {
    std::set<uint32_t> queue_set;
    std::vector<VkPipelineShaderStageCreateInfo> shader_stages;
    std::vector<Vertex> vertices;
    std::vector<uint16_t> indices;
    VkSurfaceKHR surface;
    uint32_t shader_amount;
    uint32_t queue_index;   // graphic queue
    GLFWwindow** window;
} VrulkEngineTools;

typedef struct VrulkEngine {
    VrulkPipeline* graphics_render;
    VrulkSwapchain* swapchain;
    VrulkCommands* commands;
} VrulkEngine;

/** Vrulk Engine Functions **/


VrulkEngineTools* create_vrulk_tools(std::set<uint32_t> queue_set, 
                                     std::vector<VkPipelineShaderStageCreateInfo> shader_stages, 
                                     VkSurfaceKHR surface, 
                                     uint32_t shader_amount, 
                                     uint32_t queue_index, 
                                     GLFWwindow** window,
                                     std::vector<Vertex> vertices,
                                     std::vector<uint16_t> indices
                                     );


#endif


