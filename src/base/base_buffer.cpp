#include "base_buffer.h"

#include "vrulk_utils.h"
#include <stdlib.h>

std::vector<VkDescriptorBufferInfo> create_buffer_descriptors(std::vector<VkBuffer> buffers, VkDeviceSize data_size) {
    std::vector<VkDescriptorBufferInfo> buffer_descriptors;
    buffer_descriptors.resize(buffers.size());

    for(int i = 0; i < buffers.size(); i++) {
        buffer_descriptors[i].buffer = buffers[i];
        buffer_descriptors[i].offset = 0;
        buffer_descriptors[i].range = data_size;
    }

    return buffer_descriptors;

}

VkBuffer createBuffer(VkDevice device, VkDeviceSize size, VkBufferUsageFlags usage) {
    VkBufferCreateInfo bufferInfo;
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.pNext = VK_NULL_HANDLE;
    bufferInfo.flags = 0;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    bufferInfo.queueFamilyIndexCount = 0;
    bufferInfo.pQueueFamilyIndices = nullptr;

    VkBuffer vertexBuffer;
    if (vkCreateBuffer(device, &bufferInfo, nullptr, &vertexBuffer) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    } else {
        return vertexBuffer;
    }
}

/* Utilizes a staging buffer */
VkBuffer createVertexBuffer(VkDevice device, VkDeviceSize size, VkBufferUsageFlags flags) {

    if (VK_BUFFER_USAGE_TRANSFER_DST_BIT & flags) {
        return createBuffer(device, size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT); 
    } else {
        return createBuffer(device, size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
    }
}

VkBuffer createIndexBuffer(VkDevice device , VkDeviceSize size, VkBufferUsageFlags flags) {
    return createBuffer(device, size, VK_BUFFER_USAGE_INDEX_BUFFER_BIT | flags);
}

VkDeviceMemory* allocateBufferMemory(VkPhysicalDevice physicalDevice, VkDevice device, VkMemoryPropertyFlags memory_property, VkBuffer* buffer, VkDeviceMemory* memory) {
    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(device, *buffer, &memRequirements);

    uint32_t memory_type = findMemoryType(physicalDevice, memRequirements.memoryTypeBits, memory_property);

    VkMemoryAllocateInfo allocInfo;
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.pNext = VK_NULL_HANDLE;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = memory_type;

    if (vkAllocateMemory(device, &allocInfo, nullptr, memory) != VK_SUCCESS) {
        return VK_NULL_HANDLE;
    } else {
        return memory;
    }

}

VkResult bindBuffer(VkDevice device, uint32_t size, VkBuffer* buffer, VkDeviceMemory* bufferMemory) {
    vkBindBufferMemory(device, *buffer, *bufferMemory, 0);

    /* Don't forget to return, otherwise it is undefined behaviour/ seg fault */
    return VK_SUCCESS;
}

void copyBuffer(VkDevice* device, 
                VkCommandPool commandPool, 
                VkQueue* queue, 
                VkBuffer* src_buffer, 
                VkBuffer* dstBuffer, 
                VkDeviceSize size) 
{
    VkCommandBufferAllocateInfo allocInfo;
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.pNext = VK_NULL_HANDLE;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = commandPool;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(*device, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo;
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.pNext = VK_NULL_HANDLE;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    beginInfo.pInheritanceInfo = nullptr;

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

        VkBufferCopy copyRegion;
        copyRegion.srcOffset = 0;
        copyRegion.dstOffset = 0;
        copyRegion.size = size;
        vkCmdCopyBuffer(commandBuffer, *src_buffer, *dstBuffer, 1, &copyRegion);

    vkEndCommandBuffer(commandBuffer);

    // execute command
    VkSubmitInfo submitInfo;
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.pNext = VK_NULL_HANDLE;
    submitInfo.waitSemaphoreCount = 0;
    submitInfo.pWaitSemaphores = nullptr;
    submitInfo.pWaitDstStageMask = nullptr;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;
    submitInfo.signalSemaphoreCount = 0;
    submitInfo.pSignalSemaphores = nullptr;

    vkQueueSubmit(*queue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(*queue);     // wait for completion

    vkFreeCommandBuffers(*device, commandPool, 1, &commandBuffer);
}

bool create_mapped_buffer(VkPhysicalDevice physicalDevice, VkDevice device, 
                          VkCommandPool pool, VkQueue* queue,
                          VkDeviceSize size, 
                          VkBufferUsageFlags usage_flags, VkBuffer* buffer, 
                          VkDeviceMemory* buffer_memory, const void* g_data) 
{
     VkBuffer staging_buffer = createBuffer(device, size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
    VkDeviceMemory* staging_buffer_memory = (VkDeviceMemory *) malloc(sizeof(VkDeviceMemory));
    allocateBufferMemory(physicalDevice, 
                         device, 
                         VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
                         &staging_buffer,
                         staging_buffer_memory);

    if (staging_buffer_memory == VK_NULL_HANDLE) {
        return false;
    }
    bindBuffer(device, size, &staging_buffer, staging_buffer_memory);
    mapData(device, staging_buffer_memory, size, g_data);

    *buffer = createBuffer(device, size, usage_flags);
    allocateBufferMemory(physicalDevice, 
                         device, 
                         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
                         buffer, 
                         buffer_memory);

    bindBuffer(device, size, buffer, buffer_memory);

    copyBuffer(&device, pool, queue, &staging_buffer, buffer, size);


    vkDestroyBuffer(device, staging_buffer, nullptr);
    vkFreeMemory(device, *staging_buffer_memory, nullptr);
    free(staging_buffer_memory);

    return true;
}
