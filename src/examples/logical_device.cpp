#include "../base/base_vrulk.h"

const char * app_name = "Logical Device - Example";
const char * engine_name = "Logical Device Engine";
uint32_t app_version = 1;
uint32_t engine_version = 1;

std::vector<const char *> instance_extensions;
std::vector<const char *> device_extensions;
std::vector<struct QueueFamily> extension_queues;

int main()
{
    VkInstance instance = createInstance(app_name, app_version,
                                         engine_name, engine_version,
                                         instance_extensions,
                                         true, nullptr);
    
    VkPhysicalDevice physicalDevice = createPhysicalDevice(instance, nullptr);
    VkDevice device = createLogicalDevice(physicalDevice,
                                          extension_queues,
                                          device_extensions,
                                          nullptr);
    
    vkDestroyDevice(device, nullptr);
    vkDestroyInstance(instance, nullptr);
}
